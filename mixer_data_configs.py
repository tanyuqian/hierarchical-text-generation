configs = dict()

configs['iwslt14'] = {
    'train': {
        'num_epochs': 1,
        'batch_size': 32,
        'allow_smaller_final_batch': False,
        'source_dataset': {
            "files": ['data/iwslt14/train.de'],
            'vocab_file': 'data/iwslt14/vocab.de',
            'max_seq_length': 50
        },
        'target_dataset': {
            'files': ['data/iwslt14/train.en'],
            'vocab_file': 'data/iwslt14/vocab.en',
            'max_seq_length': 50,
            "eos_token": "<TARGET_EOS>"
        }
    },
    'valid': {
        'num_epochs': 1,
        'batch_size': 32,
        'allow_smaller_final_batch': False,
        'source_dataset': {
            "files": ['data/iwslt14/valid.de'],
            'vocab_file': 'data/iwslt14/vocab.de'
        },
        'target_dataset': {
            'files': ['data/iwslt14/valid.en'],
            'vocab_file': 'data/iwslt14/vocab.en',
            "eos_token": "<TARGET_EOS>"
        }
    },
    'test': {
        'num_epochs': 1,
        'batch_size': 32,
        'allow_smaller_final_batch': False,
        'source_dataset': {
            "files": ['data/iwslt14/test.de'],
            'vocab_file': 'data/iwslt14/vocab.de',
        },
        'target_dataset': {
            'files': ['data/iwslt14/test.en'],
            'vocab_file': 'data/iwslt14/vocab.en',
            "eos_token": "<TARGET_EOS>"
        }
    }
}

configs['textsum'] = {
    'train': {
        'num_epochs': 1,
        'batch_size': 38,
        'allow_smaller_final_batch': False,
        'source_dataset': {
            "files": ['data/text-sum/train.article'],
            'vocab_file': 'data/text-sum/vocab.article',
        },
        'target_dataset': {
            'files': ['data/text-sum/train.title'],
            'vocab_file': 'data/text-sum/vocab.title',
            'max_seq_length': 25,
            "eos_token": "<TARGET_EOS>"
        }
    },
    'valid': {
        'num_epochs': 1,
        'batch_size': 23,
        'allow_smaller_final_batch': False,
        'source_dataset': {
            "files": ['data/text-sum/valid.article'],
            'vocab_file': 'data/text-sum/vocab.article',
        },
        'target_dataset': {
            'files': ['data/text-sum/valid.title'],
            'vocab_file': 'data/text-sum/vocab.title',
            "eos_token": "<TARGET_EOS>"
        }
    },
    'test': {
        'num_epochs': 1,
        'batch_size': 45,
        'allow_smaller_final_batch': False,
        'source_dataset': {
            "files": ['data/text-sum/test.article'],
            'vocab_file': 'data/text-sum/vocab.article'
        },
        'target_dataset': {
            'files': ['data/text-sum/test.title'],
            'vocab_file': 'data/text-sum/vocab.title',
            "eos_token": "<TARGET_EOS>"
        }
    }
}