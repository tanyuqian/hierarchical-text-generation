from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import tensorflow as tf

from texar.data import MonoTextDataBase
from texar.losses import mle_losses
from texar.core import optimization as opt
from texar import context

import sys

RNN_UNITS = 128
EMBEDDING_DIM = 100
C = 0.05


def train():
    data_hparams = {
        'num_epochs': 20,
        'batch_size': 64,
        'dataset': {
            'files': ['data/ptb/ptb.train.txt'],
            'vocab_file': 'data/ptb/vocab.txt'
        }
    }
    database = MonoTextDataBase(hparams=data_hparams)
    data_batch = database()

    rnn_cell = tf.nn.rnn_cell.BasicLSTMCell(num_units=RNN_UNITS)
    embedding = tf.get_variable(
        name='embedding', shape=(database.vocab.vocab_size, EMBEDDING_DIM))

    def cond(t, rnn_state, logits_ta, logits_follow_ta):
        return t < tf.shape(data_batch['text_ids'])[1]

    def body(t, rnn_state, logits_ta, logits_follow_ta):
        embedding_vec = tf.nn.embedding_lookup(
            embedding, data_batch['text_ids'][:, t])

        rnn_output, rnn_state_next = rnn_cell(embedding_vec, rnn_state)

        cur_logits = tf.layers.dense(
            inputs=rnn_output, units=database.vocab.vocab_size)
        logits_ta = tf.cond(
            t + 1 < tf.shape(data_batch['text_ids'])[1],
            lambda: logits_ta.write(t, cur_logits),
            lambda: logits_ta
        )

        sampled_ids = tf.argmax(cur_logits, axis=1)
        sampled_embedding_vec = tf.nn.embedding_lookup(embedding, sampled_ids)

        rnn_output_follow, rnn_state_follow = rnn_cell(
            sampled_embedding_vec, rnn_state_next)
        cur_logits_follow = tf.layers.dense(
            inputs=rnn_output_follow, units=database.vocab.vocab_size)
        logits_follow_ta = tf.cond(
            t + 2 < tf.shape(data_batch['text_ids'])[1],
            lambda: logits_follow_ta.write(t, cur_logits_follow),
            lambda: logits_follow_ta
        )

        t = t + 1
        rnn_state = rnn_state_next
        return t, rnn_state, logits_ta, logits_follow_ta

    _, rnn_state, logits_ta, logits_follow_ta = tf.while_loop(
        cond, body, [
            0,
            rnn_cell.zero_state(
                batch_size=database.batch_size, dtype=tf.float32),
            tf.TensorArray(dtype=tf.float32, size=1, dynamic_size=True),
            tf.TensorArray(dtype=tf.float32, size=1, dynamic_size=True)
        ])
    logits = tf.transpose(logits_ta.stack(), perm=[1, 0, 2])
    logits_follow = tf.transpose(logits_follow_ta.stack(), perm=[1, 0, 2])

    mle_loss_main = mle_losses.average_sequence_sparse_softmax_cross_entropy(
        labels=data_batch['text_ids'][:, 1:],
        logits=logits,
        sequence_length=data_batch['length'] - 1
    )
    mle_loss_follow = mle_losses.average_sequence_sparse_softmax_cross_entropy(
        labels=data_batch['text_ids'][:, 2:],
        logits=logits_follow,
        sequence_length=data_batch['length'] - 2
    )
    mle_loss = mle_loss_main + C * mle_loss_follow

    opt_hparams = {
        "optimizer": {
            "type": "AdamOptimizer",
            "kwargs": {
                # "learning_rate": 0.01,
                # "momentum": 0.9
            }
        }
    }
    train_op, global_step = opt.get_train_op(mle_loss, hparams=opt_hparams)

    with tf.Session() as sess:
        writer = tf.summary.FileWriter("logs/", sess.graph)
        sess.run(tf.global_variables_initializer())
        sess.run(tf.local_variables_initializer())
        sess.run(tf.tables_initializer())

        coord = tf.train.Coordinator()
        threads = tf.train.start_queue_runners(sess=sess, coord=coord)

        try:
            while not coord.should_stop():
                print(sess.run(train_op, feed_dict={context.is_train(): True}))

        except tf.errors.OutOfRangeError:
            print('Done -- epoch limit reached')
        finally:
            coord.request_stop()
        coord.join(threads)

        saver = tf.train.Saver(tf.global_variables())
        saved_path = saver.save(sess, 'checkpoints/lm_c{}.ckpt'.format(C))
        print('Model saved in', saved_path)


def test():
    data_hparams = {
        'num_epochs': 1,
        'batch_size': 80,
        'dataset': {
            'files': ['data/ptb/ptb.test.txt'],
            'vocab_file': 'data/ptb/vocab.txt'
        }
    }
    database = MonoTextDataBase(hparams=data_hparams)
    data_batch = database()

    rnn_cell = tf.nn.rnn_cell.BasicLSTMCell(num_units=RNN_UNITS)
    embedding = tf.get_variable(
        name='embedding', shape=(database.vocab.vocab_size, EMBEDDING_DIM))

    def cond(t, rnn_state, logits_ta, logits_follow_ta):
        return t < tf.shape(data_batch['text_ids'])[1]

    def body(t, rnn_state, logits_ta, logits_follow_ta):
        embedding_vec = tf.nn.embedding_lookup(
            embedding, data_batch['text_ids'][:, t])

        rnn_output, rnn_state_next = rnn_cell(embedding_vec, rnn_state)

        cur_logits = tf.layers.dense(
            inputs=rnn_output, units=database.vocab.vocab_size)
        logits_ta = tf.cond(
            t + 1 < tf.shape(data_batch['text_ids'])[1],
            lambda: logits_ta.write(t, cur_logits),
            lambda: logits_ta
        )

        sampled_ids = tf.argmax(cur_logits, axis=1)
        sampled_embedding_vec = tf.nn.embedding_lookup(embedding, sampled_ids)

        rnn_output_follow, rnn_state_follow = rnn_cell(
            sampled_embedding_vec, rnn_state_next)
        cur_logits_follow = tf.layers.dense(
            inputs=rnn_output_follow, units=database.vocab.vocab_size)
        logits_follow_ta = tf.cond(
            t + 2 < tf.shape(data_batch['text_ids'])[1],
            lambda: logits_follow_ta.write(t, cur_logits_follow),
            lambda: logits_follow_ta
        )

        t = t + 1
        rnn_state = rnn_state_next
        return t, rnn_state, logits_ta, logits_follow_ta

    _, rnn_state, logits_ta, logits_follow_ta = tf.while_loop(
        cond, body, [
            0,
            rnn_cell.zero_state(
                batch_size=database.batch_size, dtype=tf.float32),
            tf.TensorArray(dtype=tf.float32, size=1, dynamic_size=True),
            tf.TensorArray(dtype=tf.float32, size=1, dynamic_size=True)
        ])
    logits = tf.transpose(logits_ta.stack(), perm=[1, 0, 2])
    logits_follow = tf.transpose(logits_follow_ta.stack(), perm=[1, 0, 2])

    mle_loss_main = mle_losses.average_sequence_sparse_softmax_cross_entropy(
        labels=data_batch['text_ids'][:, 1:],
        logits=logits,
        sequence_length=data_batch['length'] - 1
    )
    mle_loss = mle_loss_main

    result_list = []
    with tf.Session() as sess:
        sess.run(tf.global_variables_initializer())
        sess.run(tf.local_variables_initializer())
        sess.run(tf.tables_initializer())

        saver = tf.train.Saver()
        saver.restore(sess, 'checkpoints/lm_c{}.ckpt'.format(C))

        coord = tf.train.Coordinator()
        threads = tf.train.start_queue_runners(sess=sess, coord=coord)

        try:
            while not coord.should_stop():
                result_list.append(
                    sess.run(mle_loss, feed_dict={context.is_train(): False}))
                # print(result_list[-1])

        except tf.errors.OutOfRangeError:
            print('Done -- epoch limit reached')
        finally:
            coord.request_stop()
        coord.join(threads)

    result = sum(result_list) / len(result_list)
    print(result)


if __name__ == '__main__':
    if len(sys.argv) > 2:
        C = eval(sys.argv[2])

    if sys.argv[1] == 'train':
        train()
    else:
        test()
