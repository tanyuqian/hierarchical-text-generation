# coding: utf-8

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import tensorflow as tf
from tensorflow.contrib import seq2seq
from tensorflow.contrib.seq2seq import BeamSearchDecoder

from texar.data import qPairedTextData
from texar.modules import UnidirectionalRNNEncoder
from texar.modules import BidirectionalRNNEncoder
from texar.modules import get_helper
from magic_rnn_decoder import MagicRNNDecoder
from texar.losses import mle_losses
from texar.core import optimization as opt
from texar import context

from nltk.translate import bleu_score
from rouge import Rouge

import os
import sys
import math
import argparse
import json
from datetime import datetime

DATASET = None
BEAM_WIDTH = None
NUM_EPOCHS = None
LOG_DIR = None
NUM_UNITS = None
MAGIC_K = None
C = None
D = None
E = None
USE_SS = None
DECAY_K = None
N_SAMPLES = None
SAMPLE_METHOD = None
METRIC = None


def inverse_sigmoid_decay(i):
    return DECAY_K / (DECAY_K + math.exp(i / DECAY_K))


def training_model_simple(database, data_batch):
    encoder_hparams = UnidirectionalRNNEncoder.default_hparams()
    encoder_hparams['rnn_cell']['cell']['kwargs']['num_units'] = NUM_UNITS
    encoder_hparams['embedding']['dim'] = NUM_UNITS
    encoder = UnidirectionalRNNEncoder(
        vocab_size=database.source_vocab.vocab_size,
        hparams=encoder_hparams)
    enc_outputs, enc_last = encoder(inputs=data_batch['source_text_ids'])
    decoder_cell = tf.nn.rnn_cell.BasicLSTMCell(num_units=NUM_UNITS)

    return enc_last, decoder_cell


def infer_model_simple(database, data_batch):
    encoder_hparams = UnidirectionalRNNEncoder.default_hparams()
    encoder_hparams['rnn_cell']['cell']['kwargs']['num_units'] = NUM_UNITS
    encoder_hparams['embedding']['dim'] = NUM_UNITS
    encoder = UnidirectionalRNNEncoder(
        vocab_size=database.source_vocab.vocab_size,
        hparams=encoder_hparams)
    enc_outputs, enc_last = encoder(inputs=data_batch['source_text_ids'])
    decoder_cell = tf.nn.rnn_cell.BasicLSTMCell(num_units=NUM_UNITS)

    return seq2seq.tile_batch(enc_last, BEAM_WIDTH), decoder_cell


def training_model_large(database, data_batch):
    DROPOUT = 0.2
    encoder_hparams = BidirectionalRNNEncoder.default_hparams()
    encoder_hparams['rnn_cell_fw']['cell']['kwargs']['num_units'] = NUM_UNITS
    encoder_hparams['rnn_cell_fw']['dropout']['input_keep_prob'] = 1. - DROPOUT
    encoder_hparams['rnn_cell_bw']['cell']['kwargs']['num_units'] = NUM_UNITS
    encoder_hparams['rnn_cell_bw']['dropout']['input_keep_prob'] = 1. - DROPOUT
    encoder_hparams['embedding']['dim'] = NUM_UNITS
    encoder = BidirectionalRNNEncoder(
        vocab_size=database.source_vocab.vocab_size,
        hparams=encoder_hparams)
    enc_outputs, enc_last = encoder(inputs=data_batch['source_text_ids'])

    decoder_cell = tf.nn.rnn_cell.MultiRNNCell(cells=[
        tf.nn.rnn_cell.DropoutWrapper(
            cell=tf.nn.rnn_cell.BasicLSTMCell(num_units=NUM_UNITS),
            input_keep_prob=1. - DROPOUT),
        tf.nn.rnn_cell.DropoutWrapper(
            cell=tf.nn.rnn_cell.BasicLSTMCell(num_units=NUM_UNITS),
            input_keep_prob=1. - DROPOUT)
    ])
    attention_mechanism = seq2seq.LuongAttention(
        num_units=NUM_UNITS,
        memory=BidirectionalRNNEncoder.concat_outputs(enc_outputs),
        memory_sequence_length=data_batch['source_length'],
        scale=True)
    decoder_cell = seq2seq.AttentionWrapper(
        cell=decoder_cell,
        attention_mechanism=attention_mechanism,
        attention_layer_size=NUM_UNITS)

    return decoder_cell.zero_state(
        batch_size=database.batch_size, dtype=tf.float32), decoder_cell


def infer_model_large(database, data_batch):
    encoder_hparams = BidirectionalRNNEncoder.default_hparams()
    encoder_hparams['rnn_cell_fw']['cell']['kwargs']['num_units'] = NUM_UNITS
    encoder_hparams['rnn_cell_bw']['cell']['kwargs']['num_units'] = NUM_UNITS
    encoder_hparams['embedding']['dim'] = NUM_UNITS
    encoder = BidirectionalRNNEncoder(
        vocab_size=database.source_vocab.vocab_size,
        hparams=encoder_hparams)
    enc_outputs, enc_last = encoder(inputs=data_batch['source_text_ids'])

    decoder_cell = tf.nn.rnn_cell.MultiRNNCell(cells=[
        tf.nn.rnn_cell.DropoutWrapper(
            cell=tf.nn.rnn_cell.BasicLSTMCell(num_units=NUM_UNITS),
            input_keep_prob=1.),
        tf.nn.rnn_cell.DropoutWrapper(
            cell=tf.nn.rnn_cell.BasicLSTMCell(num_units=NUM_UNITS),
            input_keep_prob=1.)
    ])

    def tiled(t):
        return seq2seq.tile_batch(t, BEAM_WIDTH)

    attention_mechanism = seq2seq.LuongAttention(
        num_units=NUM_UNITS,
        memory=tiled(BidirectionalRNNEncoder.concat_outputs(enc_outputs)),
        memory_sequence_length=tiled(data_batch['source_length']),
        scale=True)
    decoder_cell = seq2seq.AttentionWrapper(
        cell=decoder_cell,
        attention_mechanism=attention_mechanism,
        attention_layer_size=NUM_UNITS)

    return decoder_cell.zero_state(batch_size=database.batch_size * BEAM_WIDTH,
                                   dtype=tf.float32), decoder_cell


def training_loss(data_batch, outputs):
    mle_loss = mle_losses.average_sequence_sparse_softmax_cross_entropy(
        labels=data_batch['target_text_ids'][:, 1:],
        logits=outputs.logits,
        sequence_length=data_batch['target_length'] - 1)

    loss_follow1 = 0
    loss_follow2 = 0
    loss_follow3 = 0
    for i in range(N_SAMPLES):
        if MAGIC_K >= 2:
            loss_follow1 = \
                loss_follow1 + \
                mle_losses.average_sequence_sparse_softmax_cross_entropy(
                    labels=data_batch['target_text_ids'][:, 2:],
                    logits=outputs.logits_follow1[:, :-1, :, i],
                    sequence_length=data_batch['target_length'] - 2) + \
                mle_losses.average_sequence_sparse_softmax_cross_entropy(
                    labels=outputs.sampled_ids0[:, :-1, i],
                    logits=outputs.logits[:, :-1, :],
                    sequence_length=data_batch['target_length'] - 2)

        if MAGIC_K >= 3:
            loss_follow2 = \
                loss_follow2 + \
                mle_losses.average_sequence_sparse_softmax_cross_entropy(
                    labels=data_batch['target_text_ids'][:, 3:],
                    logits=outputs.logits_follow2[:, :-2, :, i],
                    sequence_length=data_batch['target_length'] - 3) + \
                mle_losses.average_sequence_sparse_softmax_cross_entropy(
                    labels=outputs.sampled_ids1[:, :-2, i],
                    logits=outputs.logits_follow1[:, :-2, :, i],
                    sequence_length=data_batch['target_length'] - 3) + \
                mle_losses.average_sequence_sparse_softmax_cross_entropy(
                    labels=outputs.sampled_ids0[:, :-2, i],
                    logits=outputs.logits[:, :-2, :],
                    sequence_length=data_batch['target_length'] - 3)

        if MAGIC_K >= 4:
            loss_follow3 = \
                loss_follow3 + \
                mle_losses.average_sequence_sparse_softmax_cross_entropy(
                    labels=data_batch['target_text_ids'][:, 4:],
                    logits=outputs.logits_follow3[:, :-3, :, i],
                    sequence_length=data_batch['target_length'] - 4) + \
                mle_losses.average_sequence_sparse_softmax_cross_entropy(
                    labels=outputs.sampled_ids2[:, :-3, i],
                    logits=outputs.logits_follow2[:, :-3, :, i],
                    sequence_length=data_batch['target_length'] - 4) + \
                mle_losses.average_sequence_sparse_softmax_cross_entropy(
                    labels=outputs.sampled_ids1[:, :-3, i],
                    logits=outputs.logits_follow1[:, :-3, :, i],
                    sequence_length=data_batch['target_length'] - 4) + \
                mle_losses.average_sequence_sparse_softmax_cross_entropy(
                    labels=outputs.sampled_ids0[:, :-3, i],
                    logits=outputs.logits[:, :-3, :],
                    sequence_length=data_batch['target_length'] - 4)
    loss_follow1 = loss_follow1 / float(N_SAMPLES)
    loss_follow2 = loss_follow2 / float(N_SAMPLES)
    loss_follow3 = loss_follow3 / float(N_SAMPLES)
    return mle_loss + C * loss_follow1 + D * loss_follow2 + E * loss_follow3


def train(cur_epoch):
    cur_training_log_file = open(
        LOG_DIR + 'training_log_epoch{}.txt'.format(cur_epoch), 'w')

    training_graph = tf.Graph()
    with training_graph.as_default():
        data_hparams = json.load(
            open('data_configs/' + DATASET + '_configs.json'))['train']
        database = qPairedTextData(hparams=data_hparams)
        data_batch = database()

        if DATASET == 'toycopy':
            dec_initial_state, decoder_cell = \
                training_model_simple(database, data_batch)
        else:
            dec_initial_state, decoder_cell = \
                training_model_large(database, data_batch)

        decoder_hparams = MagicRNNDecoder.default_hparams()
        decoder_hparams['embedding']['dim'] = NUM_UNITS
        decoder_hparams['helper_train']['type'] = \
            'ScheduledEmbeddingTrainingHelper'
        decoder_hparams['n_samples'] = N_SAMPLES
        decoder_hparams['sample_method'] = SAMPLE_METHOD
        decoder_hparams['magic_length'] = MAGIC_K
        decoder = MagicRNNDecoder(
            cell=decoder_cell, vocab_size=database.target_vocab.vocab_size,
            hparams=decoder_hparams)

        sampling_proba = tf.placeholder(dtype=tf.float32, shape=[])
        helper_train = get_helper(
            decoder.hparams.helper_train.type,
            inputs=tf.nn.embedding_lookup(
                decoder.embedding, data_batch['target_text_ids'][:, :-1]),
            sequence_length=data_batch['target_length'] - 1,
            embedding=decoder.embedding,
            sampling_probability=sampling_proba)

        outputs, final_state, sequence_lengths = decoder(
            helper=helper_train, initial_state=dec_initial_state)

        total_loss = training_loss(data_batch, outputs)

        opt_hparams = {
            "optimizer": {
                "type": "AdamOptimizer",
                "kwargs": {}
            }
        }
        train_op, global_step = \
            opt.get_train_op(total_loss, hparams=opt_hparams)

        saver = tf.train.Saver(tf.global_variables())
        with tf.Session() as sess:
            if cur_epoch == 0:
                sess.run(tf.global_variables_initializer())
            else:
                saver.restore(sess, LOG_DIR +
                              'checkpoints/train.ckpt_{}'.format(cur_epoch - 1))
            sess.run(tf.local_variables_initializer())
            sess.run(tf.tables_initializer())

            coord = tf.train.Coordinator()
            threads = tf.train.start_queue_runners(sess=sess, coord=coord)

            try:
                while not coord.should_stop():
                    if USE_SS:
                        cur_global_step = sess.run(
                            global_step, feed_dict={context.is_train(): False})
                        cur_p = 1. - inverse_sigmoid_decay(cur_global_step + 1.)
                    else:
                        cur_p = 0.
                    cur_loss, cur_sampling_proba, cur_global_step = \
                        sess.run([train_op, sampling_proba, global_step],
                                 feed_dict={
                                     context.is_train(): True,
                                     sampling_proba: cur_p})
                    cur_training_log_file.write(
                        datetime.now().strftime('%Y-%m-%d\t%H:%M:%S') +
                        '\tloss: ' + str(cur_loss) +
                        '\tsampling_proba: ' + str(cur_sampling_proba) +
                        '\tglobal_step: ' + str(cur_global_step) + '\n')
                    cur_training_log_file.flush()
                    sys.stdout.flush()

            except tf.errors.OutOfRangeError:
                print('Done -- epoch limit reached')
            finally:
                coord.request_stop()
            coord.join(threads)

            save_path = saver.save(
                sess, LOG_DIR + 'checkpoints/train.ckpt_{}'.format(cur_epoch))
            print('Model saved in' + save_path)
        cur_training_log_file.close()


def valid(cur_epoch):
    cur_valid_log_file = open(
        LOG_DIR + 'valid_log_epoch{}.txt'.format(cur_epoch), 'w')

    valid_graph = tf.Graph()
    with valid_graph.as_default():
        data_hparams = json.load(
            open('data_configs/' + DATASET + '_configs.json'))['valid']
        database = qPairedTextData(hparams=data_hparams)
        data_batch = database()

        if DATASET == 'toycopy':
            dec_initial_state, decoder_cell = \
                infer_model_simple(database, data_batch)
        else:
            dec_initial_state, decoder_cell = \
                infer_model_large(database, data_batch)

        with tf.variable_scope('magic_rnn_decoder'):
            decoder = BeamSearchDecoder(
                cell=decoder_cell,
                embedding=tf.get_variable(
                    name='embedding',
                    shape=[database.target_vocab.vocab_size, NUM_UNITS]),
                start_tokens=[database.target_vocab.bos_token_id
                              ] * database.batch_size,
                end_token=database.target_vocab.eos_token_id,
                beam_width=BEAM_WIDTH,
                initial_state=dec_initial_state,
                output_layer=tf.layers.Dense(database.target_vocab.vocab_size)
            )
            outputs, _, _ = seq2seq.dynamic_decode(
                decoder=decoder, maximum_iterations=60)

        corpus = []
        gene_texts = []
        with tf.Session() as sess:
            # sess.run(tf.global_variables_initializer())
            sess.run(tf.local_variables_initializer())
            sess.run(tf.tables_initializer())

            saver = tf.train.Saver(tf.global_variables())
            saver.restore(
                sess, LOG_DIR + 'checkpoints/train.ckpt_{}'.format(cur_epoch))

            coord = tf.train.Coordinator()
            threads = tf.train.start_queue_runners(sess=sess, coord=coord)

            try:
                while not coord.should_stop():
                    gene_ids, target_text = \
                        sess.run(
                            [outputs.predicted_ids[:, :, 0],
                             data_batch['target_text']],
                            feed_dict={context.is_train(): False})

                    for i in range(database.batch_size):
                        cur_text = []
                        for j in range(1, len(target_text[i])):
                            if target_text[i][j] == \
                                    database.target_vocab.eos_token:
                                break
                            else:
                                cur_text.append(target_text[i][j])
                        if METRIC == 'bleu':
                            corpus.append([cur_text, ])
                        elif METRIC == 'rouge':
                            corpus.append(' '.join(cur_text).decode('utf-8'))
                    for i in range(database.batch_size):
                        cur_text = []
                        for j in range(len(gene_ids[i])):
                            if gene_ids[i][j] == \
                                    database.target_vocab.eos_token_id:
                                break
                            else:
                                cur_text.append(
                                    database.target_vocab.id_to_token_map_py[
                                        gene_ids[i][j]])
                        if METRIC == 'bleu':
                            gene_texts.append(cur_text)
                        elif METRIC == 'rouge':
                            gene_texts.append(
                                ' '.join(cur_text).decode('utf-8'))
            except tf.errors.OutOfRangeError:
                print('Done -- epoch limit reached')
            finally:
                coord.request_stop()
            coord.join(threads)

        if METRIC == 'bleu':
            print('---------------------------------------')
            for i in range(len(corpus)):
                for j in range(len(corpus[i])):
                    print(corpus[i][j])
                print(gene_texts[i])
                sys.stdout.flush()
            print('---------------------------------------')
            sys.stdout.flush()
            score = 100 * bleu_score.corpus_bleu(corpus, gene_texts)
            cur_valid_log_file.write(
                'valid BLEU after epoch{}: '.format(cur_epoch) +
                str(score) + '\n')
            cur_valid_log_file.close()
            return score
        elif METRIC == 'rouge':
            print('---------------------------------------')
            for i in range(len(corpus)):
                print(corpus[i].replace(u'\xa0', u' '))
                print(gene_texts[i].replace(u'\xa0', u' '))
                sys.stdout.flush()
            print('---------------------------------------')
            sys.stdout.flush()

            rouge = Rouge()
            score = rouge.get_scores(
                hyps=gene_texts, refs=corpus, avg=True)
            cur_valid_log_file.write(
                'valid ROUGE-2 after epoch{}: '.format(cur_epoch) +
                str(score) + '\n')
            cur_valid_log_file.close()
            return score


def test(cur_epoch):
    cur_testing_log_file = open(
        LOG_DIR + 'testing_log_epoch{}.txt'.format(cur_epoch), 'w')

    testing_graph = tf.Graph()
    with testing_graph.as_default():
        data_hparams = json.load(
            open('data_configs/' + DATASET + '_configs.json'))['test']
        database = qPairedTextData(hparams=data_hparams)
        data_batch = database()

        if DATASET == 'toycopy':
            dec_initial_state, decoder_cell = \
                infer_model_simple(database, data_batch)
        else:
            dec_initial_state, decoder_cell = \
                infer_model_large(database, data_batch)

        with tf.variable_scope('magic_rnn_decoder'):
            decoder = BeamSearchDecoder(
                cell=decoder_cell,
                embedding=tf.get_variable(
                    name='embedding',
                    shape=[database.target_vocab.vocab_size, NUM_UNITS]),
                start_tokens=[database.target_vocab.bos_token_id
                              ] * database.batch_size,
                end_token=database.target_vocab.eos_token_id,
                beam_width=BEAM_WIDTH,
                initial_state=dec_initial_state,
                output_layer=tf.layers.Dense(database.target_vocab.vocab_size)
            )
            outputs, _, _ = seq2seq.dynamic_decode(
                decoder=decoder, maximum_iterations=60)

        corpus = []
        gene_texts = []
        with tf.Session() as sess:
            # sess.run(tf.global_variables_initializer())
            sess.run(tf.local_variables_initializer())
            sess.run(tf.tables_initializer())

            saver = tf.train.Saver(tf.global_variables())
            saver.restore(
                sess, LOG_DIR + 'checkpoints/train.ckpt_{}'.format(cur_epoch))

            coord = tf.train.Coordinator()
            threads = tf.train.start_queue_runners(sess=sess, coord=coord)

            try:
                while not coord.should_stop():
                    gene_ids, target_text = \
                        sess.run(
                            [outputs.predicted_ids[:, :, 0],
                             data_batch['target_text']],
                            feed_dict={context.is_train(): False})

                    for i in range(database.batch_size):
                        cur_text = []
                        for j in range(1, len(target_text[i])):
                            if target_text[i][j] == \
                                    database.target_vocab.eos_token:
                                break
                            else:
                                cur_text.append(target_text[i][j])
                        if METRIC == 'bleu':
                            corpus.append([cur_text, ])
                        elif METRIC == 'rouge':
                            corpus.append(' '.join(cur_text).decode('utf-8'))
                    for i in range(database.batch_size):
                        cur_text = []
                        for j in range(len(gene_ids[i])):
                            if gene_ids[i][j] == \
                                    database.target_vocab.eos_token_id:
                                break
                            else:
                                cur_text.append(
                                    database.target_vocab.id_to_token_map_py[
                                        gene_ids[i][j]])
                        if METRIC == 'bleu':
                            gene_texts.append(cur_text)
                        elif METRIC == 'rouge':
                            gene_texts.append(
                                ' '.join(cur_text).decode('utf-8'))
            except tf.errors.OutOfRangeError:
                print('Done -- epoch limit reached')
            finally:
                coord.request_stop()
            coord.join(threads)

        if METRIC == 'bleu':
            print('---------------------------------------')
            for i in range(len(corpus)):
                for j in range(len(corpus[i])):
                    print(corpus[i][j])
                print(gene_texts[i])
                sys.stdout.flush()
            print('---------------------------------------')
            sys.stdout.flush()
            score = 100 * bleu_score.corpus_bleu(corpus, gene_texts)
            cur_testing_log_file.write(
                'test BLEU after epoch{}: '.format(cur_epoch) +
                str(score) + '\n')
            cur_testing_log_file.close()
            return score
        elif METRIC == 'rouge':
            print('---------------------------------------')
            for i in range(len(corpus)):
                print(corpus[i].replace(u'\xa0', u' '))
                print(gene_texts[i].replace(u'\xa0', u' '))
                sys.stdout.flush()
            print('---------------------------------------')
            sys.stdout.flush()

            rouge = Rouge()
            score = rouge.get_scores(
                hyps=gene_texts, refs=corpus, avg=True)
            cur_testing_log_file.write(
                'test ROUGE-2 after epoch{}: '.format(cur_epoch) +
                str(score) + '\n')
            cur_testing_log_file.close()
            return score


def parse_args():
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument(
        '--dataset', type=str, default=None, help='iwslt14|textsum|toycopy')
    arg_parser.add_argument('--k', type=int, default=1)
    arg_parser.add_argument('--c', type=float, default=0.)
    arg_parser.add_argument('--d', type=float, default=0.)
    arg_parser.add_argument('--e', type=float, default=0.)
    arg_parser.add_argument('--use_ss', type=bool, default=False)
    arg_parser.add_argument('--decay', type=float, default=3000.)
    arg_parser.add_argument('--n_samples', type=int, default=1)
    arg_parser.add_argument(
        '--sample_method',
        type=str, default='argmax', help='argmax|categorical')
    arg_parser.add_argument(
        '--metric', type=str, default='bleu', help='bleu|rouge')

    return arg_parser.parse_args()


if __name__ == '__main__':
    args = parse_args()

    DATASET = args.dataset
    MAGIC_K = args.k
    C = args.c
    D = args.d
    E = args.e
    USE_SS = args.use_ss
    DECAY_K = args.decay
    N_SAMPLES = args.n_samples
    SAMPLE_METHOD = args.sample_method
    METRIC = args.metric

    if DATASET == 'toycopy':
        NUM_UNITS = 128
        NUM_EPOCHS = 400
        BEAM_WIDTH = 4
    else:
        NUM_UNITS = 512
        BEAM_WIDTH = 10
        if DATASET == 'iwslt14':
            NUM_EPOCHS = 10
        else:
            NUM_EPOCHS = 20

    LOG_DIR = DATASET + '_training_log' + '_K' + str(MAGIC_K) + \
              '_C' + str(C) + '_D' + str(D) + '_E' + str(E) + \
              '_sample' + str(N_SAMPLES) + '_' + SAMPLE_METHOD + \
              '_ss' + str(USE_SS)
    if USE_SS:
        LOG_DIR += '_dec' + str(DECAY_K)
    LOG_DIR += '/'

    os.system('mkdir ' + LOG_DIR)
    results_file = open(LOG_DIR + 'results.txt', 'w')

    max_valid_score = 0.
    for i in range(NUM_EPOCHS):
        train(i)

        if METRIC == 'bleu':
            scores = []
            results_file.write('valid epoch {}'.format(i) + '\n')
            for j in range(3):
                scores.append(valid(i))
                results_file.write(str(scores[-1]) + '\n')
                results_file.flush()
            max_valid_score = max(max_valid_score, sum(scores) / 3.)
            results_file.write('avg score: ' + str(sum(scores) / 3.) +
                               ' max ever: ' + str(max_valid_score) + '\n')
            results_file.write('------------------------------\n')
            results_file.flush()

            scores = []
            results_file.write('test epoch {}'.format(i) + '\n')
            for j in range(3):
                scores.append(test(i))
                results_file.write(str(scores[-1]) + '\n')
                results_file.flush()
            results_file.write('avg score: ' + str(sum(scores) / 3.) + '\n')
            results_file.write('------------------------------\n')
            results_file.flush()
        else:
            score = None
            results_file.write('valid epoch {}'.format(i) + '\n')
            for j in range(3):
                cur_score = valid(i)
                for t1 in ['rouge-1', 'rouge-2', 'rouge-l']:
                    results_file.write('\t' + t1 + '-> ')
                    for t2 in ['p', 'r', 'f']:
                        results_file.write(
                            '\t' + t2 + ': ' + str(cur_score[t1][t2]))
                    results_file.write('\n')
                results_file.flush()
                if j == 0:
                    score = cur_score
                    for t1 in ['rouge-1', 'rouge-2', 'rouge-l']:
                        for t2 in ['p', 'r', 'f']:
                            score[t1][t2] /= 3.
                else:
                    for t1 in ['rouge-1', 'rouge-2', 'rouge-l']:
                        for t2 in ['p', 'r', 'f']:
                            score[t1][t2] += cur_score[t1][t2] / 3.
            for t1 in ['rouge-1', 'rouge-2', 'rouge-l']:
                results_file.write('avg ' + t1 + '-> ')
                for t2 in ['p', 'r', 'f']:
                    results_file.write(
                        '\t' + t2 + ': ' + str(100. * score[t1][t2]))
                results_file.write('\n')
            results_file.flush()

            score = None
            results_file.write('test epoch {}'.format(i) + '\n')
            for j in range(3):
                cur_score = test(i)
                for t1 in ['rouge-1', 'rouge-2', 'rouge-l']:
                    results_file.write('\t' + t1 + '-> ')
                    for t2 in ['p', 'r', 'f']:
                        results_file.write(
                            '\t' + t2 + ': ' + str(cur_score[t1][t2]))
                    results_file.write('\n')
                results_file.flush()
                if j == 0:
                    score = cur_score
                    for t1 in ['rouge-1', 'rouge-2', 'rouge-l']:
                        for t2 in ['p', 'r', 'f']:
                            score[t1][t2] /= 3.
                else:
                    for t1 in ['rouge-1', 'rouge-2', 'rouge-l']:
                        for t2 in ['p', 'r', 'f']:
                            score[t1][t2] += cur_score[t1][t2] / 3.
            for t1 in ['rouge-1', 'rouge-2', 'rouge-l']:
                results_file.write('avg ' + t1 + '-> ')
                for t2 in ['p', 'r', 'f']:
                    results_file.write(
                        '\t' + t2 + ': ' + str(100. * score[t1][t2]))
                results_file.write('\n')
            results_file.write('------------------------------------------\n')
            results_file.flush()
