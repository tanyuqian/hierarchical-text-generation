import json

iwslt_configs = {
    'train': {
            'num_epochs': 1,
            'batch_size': 32,
            'source_dataset': {
                "files": ['data/iwslt14/train.de'],
                'vocab_file': 'data/iwslt14/vocab.de',
                'processing': {
                    'max_seq_length': 50
                }
            },
            'target_dataset': {
                'files': ['data/iwslt14/train.en'],
                'vocab_file': 'data/iwslt14/vocab.en',
                'processing': {
                    'max_seq_length': 50,
                    "eos_token": "<TARGET_EOS>"
                }
            }
        },
    'valid': {
            'num_epochs': 1,
            'batch_size': 57,
            'source_dataset': {
                "files": ['data/iwslt14/valid.de'],
                'vocab_file': 'data/iwslt14/vocab.de',
                'processing': {
                    # 'max_seq_length': 100
                }
            },
            'target_dataset': {
                'files': ['data/iwslt14/valid.en'],
                'vocab_file': 'data/iwslt14/vocab.en',
                'processing': {
                    "eos_token": "<TARGET_EOS>"
                }
            }
        },
    'test': {
            'num_epochs': 1,
            'batch_size': 29,
            'source_dataset': {
                "files": ['data/iwslt14/test.de'],
                'vocab_file': 'data/iwslt14/vocab.de',
                'processing': {
                    # 'max_seq_length': 100
                }
            },
            'target_dataset': {
                'files': ['data/iwslt14/test.en'],
                'vocab_file': 'data/iwslt14/vocab.en',
                'processing': {
                    "eos_token": "<TARGET_EOS>"
                }
            }
        }
}

textsum_configs = {
    'train': {
            'num_epochs': 1,
            'batch_size': 38,
            'source_dataset': {
                "files": ['data/text-sum/train.article'],
                'vocab_file': 'data/text-sum/vocab.article',
            },
            'target_dataset': {
                'files': ['data/text-sum/train.title'],
                'vocab_file': 'data/text-sum/vocab.title',
                'processing': {
                    'max_seq_length': 25,
                    "eos_token": "<TARGET_EOS>"
                }
            }
        },
    'valid': {
            'num_epochs': 1,
            'batch_size': 23,
            'source_dataset': {
                "files": ['data/text-sum/valid.article'],
                'vocab_file': 'data/text-sum/vocab.article',
                'processing': {
                    # 'max_seq_length': 100
                }
            },
            'target_dataset': {
                'files': ['data/text-sum/valid.title'],
                'vocab_file': 'data/text-sum/vocab.title',
                'processing': {
                    "eos_token": "<TARGET_EOS>"
                }
            }
        },
    'test': {
            'num_epochs': 1,
            'batch_size': 45,
            'source_dataset': {
                "files": ['data/text-sum/test.article'],
                'vocab_file': 'data/text-sum/vocab.article',
                'processing': {
                    # 'max_seq_length': 100
                }
            },
            'target_dataset': {
                'files': ['data/text-sum/test.title'],
                'vocab_file': 'data/text-sum/vocab.title',
                'processing': {
                    "eos_token": "<TARGET_EOS>"
                }
            }
        }
}

toycopy_configs = {
    'train': {
            'num_epochs': 1,
            'batch_size': 50,
            'source_dataset': {
                "files": ['data/toy_copy/train/sources.txt'],
                'vocab_file': 'data/toy_copy/train/vocab.sources.txt',
            },
            'target_dataset': {
                'files': ['data/toy_copy/train/targets.txt'],
                'vocab_file': 'data/toy_copy/train/vocab.targets.txt',
                'processing': {
                    'max_seq_length': 22,
                    "eos_token": "<TARGET_EOS>"
                }
            }
        },
    'valid': {
            'num_epochs': 1,
            'batch_size': 50,
            'source_dataset': {
                "files": ['data/toy_copy/dev/sources.txt'],
                'vocab_file': 'data/toy_copy/train/vocab.sources.txt',
                'processing': {
                    # 'max_seq_length': 100
                }
            },
            'target_dataset': {
                'files': ['data/toy_copy/dev/targets.txt'],
                'vocab_file': 'data/toy_copy/train/vocab.targets.txt',
                'processing': {
                    "eos_token": "<TARGET_EOS>"
                }
            }
        },
    'test': {
            'num_epochs': 1,
            'batch_size': 50,
            'source_dataset': {
                "files": ['data/toy_copy/test/sources.txt'],
                'vocab_file': 'data/toy_copy/train/vocab.sources.txt',
                'processing': {
                    # 'max_seq_length': 100
                }
            },
            'target_dataset': {
                'files': ['data/toy_copy/test/targets.txt'],
                'vocab_file': 'data/toy_copy/train/vocab.targets.txt',
                'processing': {
                    "eos_token": "<TARGET_EOS>"
                }
            }
        }
}

json.dump(iwslt_configs, open('iwslt14_configs.json', 'w'))
json.dump(textsum_configs, open('textsum_configs.json', 'w'))
json.dump(toycopy_configs, open('toycopy_configs.json', 'w'))
