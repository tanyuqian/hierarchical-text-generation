# coding: utf-8

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import tensorflow as tf
from tensorflow.contrib import seq2seq
from tensorflow.contrib.seq2seq import BeamSearchDecoder
import numpy as np

from texar.data import qPairedTextData
from texar.modules import UnidirectionalRNNEncoder
from texar.modules import BidirectionalRNNEncoder
from texar.modules import get_helper
from magic_rnn_decoder import MagicRNNDecoder
from texar.losses import mle_losses
from texar.core import optimization as opt
from texar import context

from nltk.translate import bleu_score
from rouge import Rouge

import os
import sys
import math
import argparse
import json
import random
from datetime import datetime

DATASET = None
BEAM_WIDTH = None
NUM_EPOCHS = None
LOG_DIR = None
NUM_UNITS = None
MAGIC_K = None
C = None
D = None
E = None
USE_SS = None
DECAY_K = None
N_SAMPLES = None
SAMPLE_METHOD = None
METRIC = None
SAMPLE_SIZE = 1
TAU = None


def inverse_sigmoid_decay(i):
    return DECAY_K / (DECAY_K + math.exp(i / DECAY_K))


def read_raml_sample_file():
    raml_file = open(RAML_FILE)

    training_data = []
    sample_num = -1
    for line in raml_file.readlines():
        line = line.rstrip('\n')
        # print(line)
        if line.startswith('***'):
            continue
        elif line.endswith('samples'):
            sample_num = eval(line.split()[0])
            assert sample_num == 1 or sample_num == SAMPLE_SIZE
        elif line.startswith('source:'):
            training_data.append({'source': line[7:], 'targets': []})
        else:
            training_data[-1]['targets'].append(line.split('|||'))
            if sample_num == 1:
                for i in range(SAMPLE_SIZE - 1):
                    training_data[-1]['targets'].append(line.split('|||'))
    return training_data


def training_model_simple(database, data_batch):
    encoder_hparams = UnidirectionalRNNEncoder.default_hparams()
    encoder_hparams['rnn_cell']['cell']['kwargs']['num_units'] = NUM_UNITS
    encoder_hparams['embedding']['dim'] = NUM_UNITS
    encoder = UnidirectionalRNNEncoder(
        vocab_size=database.source_vocab.vocab_size,
        hparams=encoder_hparams)
    enc_outputs, enc_last = encoder(inputs=data_batch['source_text_ids'])
    decoder_cell = tf.nn.rnn_cell.BasicLSTMCell(num_units=NUM_UNITS)

    return enc_last, decoder_cell


def infer_model_simple(database, data_batch):
    encoder_hparams = UnidirectionalRNNEncoder.default_hparams()
    encoder_hparams['rnn_cell']['cell']['kwargs']['num_units'] = NUM_UNITS
    encoder_hparams['embedding']['dim'] = NUM_UNITS
    encoder = UnidirectionalRNNEncoder(
        vocab_size=database.source_vocab.vocab_size,
        hparams=encoder_hparams)
    enc_outputs, enc_last = encoder(inputs=data_batch['source_text_ids'])
    decoder_cell = tf.nn.rnn_cell.BasicLSTMCell(num_units=NUM_UNITS)

    return seq2seq.tile_batch(enc_last, BEAM_WIDTH), decoder_cell


def training_model_large(database, data_batch):
    DROPOUT = 0.2
    encoder_hparams = BidirectionalRNNEncoder.default_hparams()
    encoder_hparams['rnn_cell_fw']['cell']['kwargs']['num_units'] = NUM_UNITS
    encoder_hparams['rnn_cell_fw']['dropout']['input_keep_prob'] = 1. - DROPOUT
    encoder_hparams['rnn_cell_bw']['cell']['kwargs']['num_units'] = NUM_UNITS
    encoder_hparams['rnn_cell_bw']['dropout']['input_keep_prob'] = 1. - DROPOUT
    encoder_hparams['embedding']['dim'] = NUM_UNITS
    encoder = BidirectionalRNNEncoder(
        vocab_size=database.source_vocab.vocab_size,
        hparams=encoder_hparams)
    enc_outputs, enc_last = encoder(inputs=data_batch['source_text_ids'])

    decoder_cell = tf.nn.rnn_cell.MultiRNNCell(cells=[
        tf.nn.rnn_cell.DropoutWrapper(
            cell=tf.nn.rnn_cell.BasicLSTMCell(num_units=NUM_UNITS),
            input_keep_prob=1. - DROPOUT),
        tf.nn.rnn_cell.DropoutWrapper(
            cell=tf.nn.rnn_cell.BasicLSTMCell(num_units=NUM_UNITS),
            input_keep_prob=1. - DROPOUT)
    ])
    attention_mechanism = seq2seq.LuongAttention(
        num_units=NUM_UNITS,
        memory=BidirectionalRNNEncoder.concat_outputs(enc_outputs),
        memory_sequence_length=data_batch['source_length'],
        scale=True)
    decoder_cell = seq2seq.AttentionWrapper(
        cell=decoder_cell,
        attention_mechanism=attention_mechanism,
        attention_layer_size=NUM_UNITS)

    return decoder_cell.zero_state(
        batch_size=database.batch_size, dtype=tf.float32), decoder_cell


def infer_model_large(database, data_batch):
    encoder_hparams = BidirectionalRNNEncoder.default_hparams()
    encoder_hparams['rnn_cell_fw']['cell']['kwargs']['num_units'] = NUM_UNITS
    encoder_hparams['rnn_cell_bw']['cell']['kwargs']['num_units'] = NUM_UNITS
    encoder_hparams['embedding']['dim'] = NUM_UNITS
    encoder = BidirectionalRNNEncoder(
        vocab_size=database.source_vocab.vocab_size,
        hparams=encoder_hparams)
    enc_outputs, enc_last = encoder(inputs=data_batch['source_text_ids'])

    decoder_cell = tf.nn.rnn_cell.MultiRNNCell(cells=[
        tf.nn.rnn_cell.DropoutWrapper(
            cell=tf.nn.rnn_cell.BasicLSTMCell(num_units=NUM_UNITS),
            input_keep_prob=1.),
        tf.nn.rnn_cell.DropoutWrapper(
            cell=tf.nn.rnn_cell.BasicLSTMCell(num_units=NUM_UNITS),
            input_keep_prob=1.)
    ])

    def tiled(t):
        return seq2seq.tile_batch(t, BEAM_WIDTH)

    attention_mechanism = seq2seq.LuongAttention(
        num_units=NUM_UNITS,
        memory=tiled(BidirectionalRNNEncoder.concat_outputs(enc_outputs)),
        memory_sequence_length=tiled(data_batch['source_length']),
        scale=True)
    decoder_cell = seq2seq.AttentionWrapper(
        cell=decoder_cell,
        attention_mechanism=attention_mechanism,
        attention_layer_size=NUM_UNITS)

    return decoder_cell.zero_state(batch_size=database.batch_size * BEAM_WIDTH,
                                   dtype=tf.float32), decoder_cell


def training_loss(data_batch, outputs):
    def average_sequence_sparse_softmax_cross_entropy(labels,
                                                      logits,
                                                      sequence_length,
                                                      rewards,
                                                      time_major=False,
                                                      name=None):
        with tf.name_scope(name,
                           "average_sequence_sparse_softmax_cross_entropy"):
            losses = tf.nn.sparse_softmax_cross_entropy_with_logits(
                labels=labels, logits=logits)
            losses = mle_losses._mask_sequences(losses, sequence_length, time_major)
            loss = tf.reduce_sum(rewards * tf.reduce_sum(losses, axis=1)) / tf.to_float(tf.shape(labels)[0]) * SAMPLE_SIZE
            return loss

    mle_loss = average_sequence_sparse_softmax_cross_entropy(
        labels=data_batch['target_text_ids'][:, 1:],
        logits=outputs.logits,
        rewards=data_batch['rewards'],
        sequence_length=data_batch['target_length'] - 1)

    loss_follow1 = 0
    loss_follow2 = 0
    loss_follow3 = 0
    for i in range(N_SAMPLES):
        if MAGIC_K >= 2:
            loss_follow1 = \
                loss_follow1 + \
                mle_losses.average_sequence_sparse_softmax_cross_entropy(
                    labels=data_batch['target_text_ids'][:, 2:],
                    logits=outputs.logits_follow1[:, :-1, :, i],
                    sequence_length=data_batch['target_length'] - 2) + \
                mle_losses.average_sequence_sparse_softmax_cross_entropy(
                    labels=outputs.sampled_ids0[:, :-1, i],
                    logits=outputs.logits[:, :-1, :],
                    sequence_length=data_batch['target_length'] - 2)

        if MAGIC_K >= 3:
            loss_follow2 = \
                loss_follow2 + \
                mle_losses.average_sequence_sparse_softmax_cross_entropy(
                    labels=data_batch['target_text_ids'][:, 3:],
                    logits=outputs.logits_follow2[:, :-2, :, i],
                    sequence_length=data_batch['target_length'] - 3) + \
                mle_losses.average_sequence_sparse_softmax_cross_entropy(
                    labels=outputs.sampled_ids1[:, :-2, i],
                    logits=outputs.logits_follow1[:, :-2, :, i],
                    sequence_length=data_batch['target_length'] - 3) + \
                mle_losses.average_sequence_sparse_softmax_cross_entropy(
                    labels=outputs.sampled_ids0[:, :-2, i],
                    logits=outputs.logits[:, :-2, :],
                    sequence_length=data_batch['target_length'] - 3)

        if MAGIC_K >= 4:
            loss_follow3 = \
                loss_follow3 + \
                mle_losses.average_sequence_sparse_softmax_cross_entropy(
                    labels=data_batch['target_text_ids'][:, 4:],
                    logits=outputs.logits_follow3[:, :-3, :, i],
                    sequence_length=data_batch['target_length'] - 4) + \
                mle_losses.average_sequence_sparse_softmax_cross_entropy(
                    labels=outputs.sampled_ids2[:, :-3, i],
                    logits=outputs.logits_follow2[:, :-3, :, i],
                    sequence_length=data_batch['target_length'] - 4) + \
                mle_losses.average_sequence_sparse_softmax_cross_entropy(
                    labels=outputs.sampled_ids1[:, :-3, i],
                    logits=outputs.logits_follow1[:, :-3, :, i],
                    sequence_length=data_batch['target_length'] - 4) + \
                mle_losses.average_sequence_sparse_softmax_cross_entropy(
                    labels=outputs.sampled_ids0[:, :-3, i],
                    logits=outputs.logits[:, :-3, :],
                    sequence_length=data_batch['target_length'] - 4)
    loss_follow1 = loss_follow1 / float(N_SAMPLES)
    loss_follow2 = loss_follow2 / float(N_SAMPLES)
    loss_follow3 = loss_follow3 / float(N_SAMPLES)
    return mle_loss + C * loss_follow1 + D * loss_follow2 + E * loss_follow3


def train(cur_epoch, training_data):
    random.shuffle(training_data)

    cur_training_log_file = open(
        LOG_DIR + 'training_log_epoch{}.txt'.format(cur_epoch), 'w')

    training_graph = tf.Graph()
    with training_graph.as_default():
        data_hparams = json.load(
            open('data_configs/' + DATASET + '_configs.json'))['train']
        data_hparams['batch_size'] *= SAMPLE_SIZE
        database = qPairedTextData(hparams=data_hparams)

        data_batch_ori = database()
        data_batch = {}
        for key, value in data_batch_ori.items():
            data_batch[key] = \
                tf.placeholder(dtype=value.dtype, shape=value.shape)
        data_batch['rewards'] = \
            tf.placeholder(dtype=tf.float32, shape=[database.batch_size, ])

        if DATASET == 'toycopy':
            dec_initial_state, decoder_cell = \
                training_model_simple(database, data_batch)
        else:
            dec_initial_state, decoder_cell = \
                training_model_large(database, data_batch)

        decoder_hparams = MagicRNNDecoder.default_hparams()
        decoder_hparams['embedding']['dim'] = NUM_UNITS
        decoder_hparams['helper_train']['type'] = \
            'ScheduledEmbeddingTrainingHelper'
        decoder_hparams['n_samples'] = N_SAMPLES
        decoder_hparams['sample_method'] = SAMPLE_METHOD
        decoder_hparams['magic_length'] = MAGIC_K
        decoder = MagicRNNDecoder(
            cell=decoder_cell, vocab_size=database.target_vocab.vocab_size,
            hparams=decoder_hparams)

        sampling_proba = tf.placeholder(dtype=tf.float32, shape=[])
        helper_train = get_helper(
            decoder.hparams.helper_train.type,
            inputs=tf.nn.embedding_lookup(
                decoder.embedding, data_batch['target_text_ids'][:, :-1]),
            sequence_length=data_batch['target_length'] - 1,
            embedding=decoder.embedding,
            sampling_probability=sampling_proba)

        outputs, final_state, sequence_lengths = decoder(
            helper=helper_train, initial_state=dec_initial_state)

        total_loss = training_loss(data_batch, outputs)

        opt_hparams = {
            "optimizer": {
                "type": "AdamOptimizer",
                "kwargs": {}
            }
        }
        train_op, global_step = \
            opt.get_train_op(total_loss, hparams=opt_hparams)

        saver = tf.train.Saver(tf.global_variables())
        with tf.Session() as sess:
            if cur_epoch == 0:
                sess.run(tf.global_variables_initializer())
            else:
                saver.restore(sess, LOG_DIR +
                              'checkpoints/train.ckpt_{}'.format(cur_epoch - 1))
            sess.run(tf.local_variables_initializer())
            sess.run(tf.tables_initializer())

            source_buffer = []
            target_buffer = []
            for training_pair in training_data:
                for target in training_pair['targets']:
                    source_buffer.append(training_pair['source'])
                    target_buffer.append(target)

                if len(target_buffer) != database.batch_size:
                    continue

                if USE_SS:
                    cur_global_step = sess.run(
                        global_step, feed_dict={context.is_train(): False})
                    cur_p = 1. - inverse_sigmoid_decay(cur_global_step + 1.)
                else:
                    cur_p = 0.

                source_ids = []
                source_length = []
                target_ids = []
                target_length = []
                scores = []

                for sentence in source_buffer:
                    ids = [database.source_vocab.token_to_id_map_py[token] for token in sentence.split()]
                    ids = ids + [database.source_vocab.eos_token_id]
                    source_ids.append(ids)
                    source_length.append(len(ids))
                for sentence, score_str in target_buffer:
                    ids = [database.target_vocab.bos_token_id]
                    ids = ids + [database.target_vocab.token_to_id_map_py[token] for token in sentence.split()]
                    ids = ids + [database.target_vocab.eos_token_id]

                    target_ids.append(ids)
                    scores.append(eval(score_str))
                    target_length.append(len(ids))

                rewards = []
                for i in range(0, database.batch_size, SAMPLE_SIZE):
                    tmp = np.array(scores[i:i + SAMPLE_SIZE])
                    rewards.append(np.exp(tmp / TAU) / np.sum(np.exp(tmp / TAU)))

                reward_norm = []
                for i in range(database.batch_size // SAMPLE_SIZE):
                    for j in range(0, SAMPLE_SIZE):
                        reward_norm.append(rewards[i][j])

                for value in source_ids:
                    while len(value) < max(source_length):
                        value.append(0)
                for value in target_ids:
                    while len(value) < max(target_length):
                        value.append(0)

                feed_dict = {context.is_train(): True, sampling_proba: cur_p}
                feed_dict[data_batch['source_text_ids']] = np.array(source_ids)
                feed_dict[data_batch['target_text_ids']] = np.array(target_ids)
                feed_dict[data_batch['source_length']] = np.array(source_length)
                feed_dict[data_batch['target_length']] = np.array(target_length)
                feed_dict[data_batch['rewards']] = np.array(reward_norm)
                source_buffer = []
                target_buffer = []

                cur_loss, cur_sampling_proba, cur_global_step = \
                    sess.run([train_op, sampling_proba, global_step],
                             feed_dict=feed_dict)

                cur_training_log_file.write(
                    datetime.now().strftime('%Y-%m-%d\t%H:%M:%S') +
                    '\tloss: ' + str(cur_loss) +
                    '\tsampling_proba: ' + str(cur_sampling_proba) +
                    '\tglobal_step: ' + str(cur_global_step) + '\n')
                cur_training_log_file.flush()
                sys.stdout.flush()

            save_path = saver.save(
                sess, LOG_DIR + 'checkpoints/train.ckpt_{}'.format(cur_epoch))
            print('Model saved in' + save_path)
        cur_training_log_file.close()


def valid(cur_epoch):
    cur_valid_log_file = open(
        LOG_DIR + 'valid_log_epoch{}.txt'.format(cur_epoch), 'w')

    valid_graph = tf.Graph()
    with valid_graph.as_default():
        data_hparams = json.load(
            open('data_configs/' + DATASET + '_configs.json'))['valid']
        database = qPairedTextData(hparams=data_hparams)
        data_batch = database()

        if DATASET == 'toycopy':
            dec_initial_state, decoder_cell = \
                infer_model_simple(database, data_batch)
        else:
            dec_initial_state, decoder_cell = \
                infer_model_large(database, data_batch)

        with tf.variable_scope('magic_rnn_decoder'):
            decoder = BeamSearchDecoder(
                cell=decoder_cell,
                embedding=tf.get_variable(
                    name='embedding',
                    shape=[database.target_vocab.vocab_size, NUM_UNITS]),
                start_tokens=[database.target_vocab.bos_token_id
                              ] * database.batch_size,
                end_token=database.target_vocab.eos_token_id,
                beam_width=BEAM_WIDTH,
                initial_state=dec_initial_state,
                output_layer=tf.layers.Dense(database.target_vocab.vocab_size)
            )
            outputs, _, _ = seq2seq.dynamic_decode(
                decoder=decoder, maximum_iterations=60)

        corpus = []
        gene_texts = []
        with tf.Session() as sess:
            # sess.run(tf.global_variables_initializer())
            sess.run(tf.local_variables_initializer())
            sess.run(tf.tables_initializer())

            saver = tf.train.Saver(tf.global_variables())
            saver.restore(
                sess, LOG_DIR + 'checkpoints/train.ckpt_{}'.format(cur_epoch))

            coord = tf.train.Coordinator()
            threads = tf.train.start_queue_runners(sess=sess, coord=coord)

            try:
                while not coord.should_stop():
                    gene_ids, target_text = \
                        sess.run(
                            [outputs.predicted_ids[:, :, 0],
                             data_batch['target_text']],
                            feed_dict={context.is_train(): False})

                    for i in range(database.batch_size):
                        cur_text = []
                        for j in range(1, len(target_text[i])):
                            if target_text[i][j] == \
                                    database.target_vocab.eos_token:
                                break
                            else:
                                cur_text.append(target_text[i][j])
                        if METRIC == 'bleu':
                            corpus.append([cur_text, ])
                        elif METRIC == 'rouge':
                            corpus.append(' '.join(cur_text).decode('utf-8'))
                    for i in range(database.batch_size):
                        cur_text = []
                        for j in range(len(gene_ids[i])):
                            if gene_ids[i][j] == \
                                    database.target_vocab.eos_token_id:
                                break
                            else:
                                cur_text.append(
                                    database.target_vocab.id_to_token_map_py[
                                        gene_ids[i][j]])
                        if METRIC == 'bleu':
                            gene_texts.append(cur_text)
                        elif METRIC == 'rouge':
                            gene_texts.append(
                                ' '.join(cur_text).decode('utf-8'))
            except tf.errors.OutOfRangeError:
                print('Done -- epoch limit reached')
            finally:
                coord.request_stop()
            coord.join(threads)

        if METRIC == 'bleu':
            print('---------------------------------------')
            for i in range(len(corpus)):
                for j in range(len(corpus[i])):
                    print(corpus[i][j])
                print(gene_texts[i])
                sys.stdout.flush()
            print('---------------------------------------')
            sys.stdout.flush()
            score = 100 * bleu_score.corpus_bleu(corpus, gene_texts)
            cur_valid_log_file.write(
                'valid BLEU after epoch{}: '.format(cur_epoch) +
                str(score) + '\n')
            cur_valid_log_file.close()
            return score
        elif METRIC == 'rouge':
            print('---------------------------------------')
            for i in range(len(corpus)):
                print(corpus[i].replace(u'\xa0', u' '))
                print(gene_texts[i].replace(u'\xa0', u' '))
                sys.stdout.flush()
            print('---------------------------------------')
            sys.stdout.flush()

            rouge = Rouge()
            score = rouge.get_scores(
                hyps=gene_texts, refs=corpus, avg=True)
            cur_valid_log_file.write(
                'valid ROUGE-2 after epoch{}: '.format(cur_epoch) +
                str(score) + '\n')
            cur_valid_log_file.close()
            return score


def test(cur_epoch):
    cur_testing_log_file = open(
        LOG_DIR + 'testing_log_epoch{}.txt'.format(cur_epoch), 'w')

    testing_graph = tf.Graph()
    with testing_graph.as_default():
        data_hparams = json.load(
            open('data_configs/' + DATASET + '_configs.json'))['test']
        database = qPairedTextData(hparams=data_hparams)
        data_batch = database()

        if DATASET == 'toycopy':
            dec_initial_state, decoder_cell = \
                infer_model_simple(database, data_batch)
        else:
            dec_initial_state, decoder_cell = \
                infer_model_large(database, data_batch)

        with tf.variable_scope('magic_rnn_decoder'):
            decoder = BeamSearchDecoder(
                cell=decoder_cell,
                embedding=tf.get_variable(
                    name='embedding',
                    shape=[database.target_vocab.vocab_size, NUM_UNITS]),
                start_tokens=[database.target_vocab.bos_token_id
                              ] * database.batch_size,
                end_token=database.target_vocab.eos_token_id,
                beam_width=BEAM_WIDTH,
                initial_state=dec_initial_state,
                output_layer=tf.layers.Dense(database.target_vocab.vocab_size)
            )
            outputs, _, _ = seq2seq.dynamic_decode(
                decoder=decoder, maximum_iterations=60)

        corpus = []
        gene_texts = []
        with tf.Session() as sess:
            # sess.run(tf.global_variables_initializer())
            sess.run(tf.local_variables_initializer())
            sess.run(tf.tables_initializer())

            saver = tf.train.Saver(tf.global_variables())
            saver.restore(
                sess, LOG_DIR + 'checkpoints/train.ckpt_{}'.format(cur_epoch))

            coord = tf.train.Coordinator()
            threads = tf.train.start_queue_runners(sess=sess, coord=coord)

            try:
                while not coord.should_stop():
                    gene_ids, target_text = \
                        sess.run(
                            [outputs.predicted_ids[:, :, 0],
                             data_batch['target_text']],
                            feed_dict={context.is_train(): False})

                    for i in range(database.batch_size):
                        cur_text = []
                        for j in range(1, len(target_text[i])):
                            if target_text[i][j] == \
                                    database.target_vocab.eos_token:
                                break
                            else:
                                cur_text.append(target_text[i][j])
                        if METRIC == 'bleu':
                            corpus.append([cur_text, ])
                        elif METRIC == 'rouge':
                            corpus.append(' '.join(cur_text).decode('utf-8'))
                    for i in range(database.batch_size):
                        cur_text = []
                        for j in range(len(gene_ids[i])):
                            if gene_ids[i][j] == \
                                    database.target_vocab.eos_token_id:
                                break
                            else:
                                cur_text.append(
                                    database.target_vocab.id_to_token_map_py[
                                        gene_ids[i][j]])
                        if METRIC == 'bleu':
                            gene_texts.append(cur_text)
                        elif METRIC == 'rouge':
                            gene_texts.append(
                                ' '.join(cur_text).decode('utf-8'))
            except tf.errors.OutOfRangeError:
                print('Done -- epoch limit reached')
            finally:
                coord.request_stop()
            coord.join(threads)

        if METRIC == 'bleu':
            print('---------------------------------------')
            for i in range(len(corpus)):
                for j in range(len(corpus[i])):
                    print(corpus[i][j])
                print(gene_texts[i])
                sys.stdout.flush()
            print('---------------------------------------')
            sys.stdout.flush()
            score = 100 * bleu_score.corpus_bleu(corpus, gene_texts)
            cur_testing_log_file.write(
                'test BLEU after epoch{}: '.format(cur_epoch) +
                str(score) + '\n')
            cur_testing_log_file.close()
            return score
        elif METRIC == 'rouge':
            print('---------------------------------------')
            for i in range(len(corpus)):
                print(corpus[i].replace(u'\xa0', u' '))
                print(gene_texts[i].replace(u'\xa0', u' '))
                sys.stdout.flush()
            print('---------------------------------------')
            sys.stdout.flush()

            rouge = Rouge()
            score = rouge.get_scores(
                hyps=gene_texts, refs=corpus, avg=True)
            cur_testing_log_file.write(
                'test ROUGE-2 after epoch{}: '.format(cur_epoch) +
                str(score) + '\n')
            cur_testing_log_file.close()
            return score


def parse_args():
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument(
        '--dataset', type=str, default=None, help='iwslt14|textsum|toycopy')
    arg_parser.add_argument('--raml_sample_file', type=str, default=None)
    arg_parser.add_argument('--tau', type=float, default=0.85)
    arg_parser.add_argument('--k', type=int, default=1)
    arg_parser.add_argument('--c', type=float, default=0.)
    arg_parser.add_argument('--d', type=float, default=0.)
    arg_parser.add_argument('--e', type=float, default=0.)
    arg_parser.add_argument('--use_ss', type=bool, default=False)
    arg_parser.add_argument('--decay', type=float, default=3000.)
    arg_parser.add_argument('--n_samples', type=int, default=1)
    arg_parser.add_argument(
        '--sample_method',
        type=str, default='argmax', help='argmax|categorical')
    arg_parser.add_argument(
        '--metric', type=str, default='bleu', help='bleu|rouge')

    return arg_parser.parse_args()


if __name__ == '__main__':
    args = parse_args()

    DATASET = args.dataset
    MAGIC_K = args.k
    C = args.c
    D = args.d
    E = args.e
    USE_SS = args.use_ss
    DECAY_K = args.decay
    N_SAMPLES = args.n_samples
    SAMPLE_METHOD = args.sample_method
    METRIC = args.metric
    RAML_FILE = args.raml_sample_file
    TAU = args.tau

    training_data = read_raml_sample_file()

    if DATASET == 'toycopy':
        NUM_UNITS = 128
        NUM_EPOCHS = 400
        BEAM_WIDTH = 4
    else:
        NUM_UNITS = 512
        BEAM_WIDTH = 10
        if DATASET == 'iwslt14':
            NUM_EPOCHS = 10
        else:
            NUM_EPOCHS = 20

    LOG_DIR = DATASET + '_training_log_raml_one' + '_K' + str(MAGIC_K) + \
              '_C' + str(C) + '_D' + str(D) + '_E' + str(E) + \
              '_sample' + str(N_SAMPLES) + '_' + SAMPLE_METHOD + \
              '_ss' + str(USE_SS)
    if USE_SS:
        LOG_DIR += '_dec' + str(DECAY_K)
    LOG_DIR += '/'

    os.system('mkdir ' + LOG_DIR)
    results_file = open(LOG_DIR + 'results.txt', 'w')

    max_valid_score = 0.
    for i in range(NUM_EPOCHS):
        train(i, training_data)

        if METRIC == 'bleu':
            scores = []
            results_file.write('valid epoch {}'.format(i) + '\n')
            for j in range(3):
                scores.append(valid(i))
                results_file.write(str(scores[-1]) + '\n')
                results_file.flush()
            max_valid_score = max(max_valid_score, sum(scores) / 3.)
            results_file.write('avg score: ' + str(sum(scores) / 3.) +
                               ' max ever: ' + str(max_valid_score) + '\n')
            results_file.write('------------------------------\n')
            results_file.flush()

            scores = []
            results_file.write('test epoch {}'.format(i) + '\n')
            for j in range(3):
                scores.append(test(i))
                results_file.write(str(scores[-1]) + '\n')
                results_file.flush()
            results_file.write('avg score: ' + str(sum(scores) / 3.) + '\n')
            results_file.write('------------------------------\n')
            results_file.flush()
        else:
            score = None
            results_file.write('valid epoch {}'.format(i) + '\n')
            for j in range(3):
                cur_score = valid(i)
                for t1 in ['rouge-1', 'rouge-2', 'rouge-l']:
                    results_file.write('\t' + t1 + '-> ')
                    for t2 in ['p', 'r', 'f']:
                        results_file.write(
                            '\t' + t2 + ': ' + str(cur_score[t1][t2]))
                    results_file.write('\n')
                results_file.flush()
                if j == 0:
                    score = cur_score
                    for t1 in ['rouge-1', 'rouge-2', 'rouge-l']:
                        for t2 in ['p', 'r', 'f']:
                            score[t1][t2] /= 3.
                else:
                    for t1 in ['rouge-1', 'rouge-2', 'rouge-l']:
                        for t2 in ['p', 'r', 'f']:
                            score[t1][t2] += cur_score[t1][t2] / 3.
            for t1 in ['rouge-1', 'rouge-2', 'rouge-l']:
                results_file.write('avg ' + t1 + '-> ')
                for t2 in ['p', 'r', 'f']:
                    results_file.write(
                        '\t' + t2 + ': ' + str(100. * score[t1][t2]))
                results_file.write('\n')
            results_file.flush()

            score = None
            results_file.write('test epoch {}'.format(i) + '\n')
            for j in range(3):
                cur_score = test(i)
                for t1 in ['rouge-1', 'rouge-2', 'rouge-l']:
                    results_file.write('\t' + t1 + '-> ')
                    for t2 in ['p', 'r', 'f']:
                        results_file.write(
                            '\t' + t2 + ': ' + str(cur_score[t1][t2]))
                    results_file.write('\n')
                results_file.flush()
                if j == 0:
                    score = cur_score
                    for t1 in ['rouge-1', 'rouge-2', 'rouge-l']:
                        for t2 in ['p', 'r', 'f']:
                            score[t1][t2] /= 3.
                else:
                    for t1 in ['rouge-1', 'rouge-2', 'rouge-l']:
                        for t2 in ['p', 'r', 'f']:
                            score[t1][t2] += cur_score[t1][t2] / 3.
            for t1 in ['rouge-1', 'rouge-2', 'rouge-l']:
                results_file.write('avg ' + t1 + '-> ')
                for t2 in ['p', 'r', 'f']:
                    results_file.write(
                        '\t' + t2 + ': ' + str(100. * score[t1][t2]))
                results_file.write('\n')
            results_file.write('------------------------------------------\n')
            results_file.flush()
