from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

import tensorflow as tf
import texar as tx
import numpy as np

from mixer_configs import configs as data_configs
from mixer_helper import MixerSampleHelper, MixerGreedyHelper

from tensorflow.contrib import seq2seq

import os
import sys
import math
import argparse
import json
from nltk.translate import bleu_score
from datetime import datetime

DROPOUT = 0.2
NUM_UNITS = 512
BEAM_WIDTH = 10

arg_parser = argparse.ArgumentParser()
arg_parser.add_argument(
    '--dataset', type=str, default=None,help='iwslt14|textsum')
arg_parser.add_argument('--n_xent', type=int, default=None)
arg_parser.add_argument('--n_xr', type=int, default=None)
arg_parser.add_argument('--length_t', type=int, default=None)
arg_parser.add_argument('--delta', type=int, default=None)
arg_parser.add_argument('--pretrained', type=str, default='False')
args = arg_parser.parse_args()

DATASET = args.dataset
N_XENT = args.n_xent
N_XR = args.n_xr
LENGTH_T = args.length_t
DELTA = args.delta
PRETRAINED = eval(args.pretrained)


def encode(database, data_batch):
    embedder = tx.modules.WordEmbedder(
        vocab_size=database.source_vocab.size, hparams={'dim': NUM_UNITS})

    encoder_hparams = tx.modules.BidirectionalRNNEncoder.default_hparams()
    encoder_hparams['rnn_cell_fw']['kwargs']['num_units'] = NUM_UNITS
    encoder_hparams['rnn_cell_fw']['dropout']['input_keep_prob'] = 1. - DROPOUT
    encoder = tx.modules.BidirectionalRNNEncoder(hparams=encoder_hparams)

    return encoder(inputs=embedder(data_batch['source_text_ids']))


def get_training_outputs(database, data_batch, enc_outputs, enc_last, rf_point):
    decoder_cell = tf.nn.rnn_cell.MultiRNNCell(cells=[
        tf.nn.rnn_cell.DropoutWrapper(
            cell=tf.nn.rnn_cell.BasicLSTMCell(num_units=NUM_UNITS),
            input_keep_prob=tx.utils.switch_dropout(1. - DROPOUT)),
        tf.nn.rnn_cell.DropoutWrapper(
            cell=tf.nn.rnn_cell.BasicLSTMCell(num_units=NUM_UNITS),
            input_keep_prob=tx.utils.switch_dropout(1. - DROPOUT))
    ])
    attention_mechanism = seq2seq.LuongAttention(
        num_units=NUM_UNITS,
        memory=tx.modules.BidirectionalRNNEncoder.concat_outputs(enc_outputs),
        memory_sequence_length=data_batch['source_length'],
        scale=True)
    decoder_cell = seq2seq.AttentionWrapper(
        cell=decoder_cell,
        attention_mechanism=attention_mechanism,
        attention_layer_size=NUM_UNITS)

    decoder = tx.modules.BasicRNNDecoder(
        cell=decoder_cell, vocab_size=database.target_vocab.size)

    embedder = tx.modules.WordEmbedder(
        vocab_size=database.target_vocab.size, hparams={'dim': NUM_UNITS})

    helper_sample = MixerSampleHelper(
        inputs=data_batch['target_text_ids'][:, 1:],
        embedding=embedder,
        start_tokens=[database.target_vocab.bos_token_id] * database.batch_size,
        end_token=database.target_vocab.eos_token_id,
        rf_point=rf_point)

    helper_greedy = MixerGreedyHelper(
        inputs=data_batch['target_text_ids'][:, 1:],
        embedding=embedder,
        start_tokens=[database.target_vocab.bos_token_id] * database.batch_size,
        end_token=database.target_vocab.eos_token_id,
        rf_point=rf_point)

    sample_outputs, _, sample_decode_lengths = decoder(
        helper=helper_sample,
        initial_state=decoder.zero_state(
            batch_size=database.batch_size, dtype=tf.float32),
        max_decoding_length=50)

    greedy_outputs, _, greedy_decode_lengths = decoder(
        helper=helper_greedy,
        initial_state=decoder.zero_state(
            batch_size=database.batch_size, dtype=tf.float32),
        max_decoding_length=50)

    return sample_outputs, greedy_outputs,\
           sample_decode_lengths, greedy_decode_lengths


def get_beam_search_outputs(database, data_batch, enc_outputs, enc_last):
    decoder_cell = tf.nn.rnn_cell.MultiRNNCell(cells=[
        tf.nn.rnn_cell.DropoutWrapper(
            cell=tf.nn.rnn_cell.BasicLSTMCell(num_units=NUM_UNITS),
            input_keep_prob=tx.utils.switch_dropout(1. - DROPOUT)),
        tf.nn.rnn_cell.DropoutWrapper(
            cell=tf.nn.rnn_cell.BasicLSTMCell(num_units=NUM_UNITS),
            input_keep_prob=tx.utils.switch_dropout(1. - DROPOUT))
    ])

    def seq2seq_tile(t):
        return seq2seq.tile_batch(t, BEAM_WIDTH)

    attention_mechanism = seq2seq.LuongAttention(
        num_units=NUM_UNITS,
        memory=seq2seq_tile(
            tx.modules.BidirectionalRNNEncoder.concat_outputs(enc_outputs)),
        memory_sequence_length=seq2seq_tile(data_batch['source_length']),
        scale=True)
    decoder_cell = seq2seq.AttentionWrapper(
        cell=decoder_cell,
        attention_mechanism=attention_mechanism,
        attention_layer_size=NUM_UNITS)

    embedder = tx.modules.WordEmbedder(
        vocab_size=database.target_vocab.size, hparams={'dim': NUM_UNITS})

    with tf.variable_scope('basic_rnn_decoder'):
        beam_search_decoder = seq2seq.BeamSearchDecoder(
            cell=decoder_cell,
            embedding=embedder,
            start_tokens=[database.target_vocab.bos_token_id
                          ] * database.batch_size,
            end_token=database.target_vocab.eos_token_id,
            beam_width=BEAM_WIDTH,
            initial_state=decoder_cell.zero_state(
                batch_size=database.batch_size * BEAM_WIDTH, dtype=tf.float32),
            output_layer=tf.layers.Dense(database.target_vocab.size))

        outputs, _, _ = seq2seq.dynamic_decode(
            decoder=beam_search_decoder, maximum_iterations=60)

    return outputs


def get_optimize_tensor(database, data_batch,
                        sample_outputs, greedy_outputs,
                        sample_decode_lengths, greedy_decode_lengths,
                        rf_point, rf_reward):
    mle_loss = tx.losses.sequence_sparse_softmax_cross_entropy(
        labels=data_batch['target_text_ids'][:, 1:rf_point + 1],
        logits=sample_outputs.logits[:, :rf_point],
        sequence_length=tf.minimum(rf_point, data_batch['target_length'] - 1))

    rf_loss = rf_reward * tx.losses.sequence_sparse_softmax_cross_entropy(
        labels=sample_outputs.sample_id[:, rf_point:],
        logits=sample_outputs.logits[:, rf_point:],
        sequence_length=tf.maximum(0, sample_decode_lengths - rf_point - 1),
        average_across_batch=False)

    return tx.core.get_train_op(
        mle_loss + tf.reduce_sum(rf_loss),
        hparams={'optimizer': {'type': 'AdamOptimizer'}})


def text_polish(texts, eos_token):
    for i in range(len(texts)):
        if texts[i] == eos_token:
            return texts[:i]
    return texts


def train(epoch_num, rf_point):
    log_file = open(LOG_DIR + 'training_log_epoch{}.txt'.format(epoch_num), 'w')

    graph = tf.Graph()
    with graph.as_default():
        database = tx.data.PairedTextData(
            hparams=data_configs[DATASET]['train'])
        iterator = tx.data.DataIterator(database)

        data_batch_ori = iterator.get_next()
        data_batch = {}
        for key, value in data_batch_ori.items():
            data_batch[key] = \
                tf.placeholder(dtype=value.dtype, shape=value.shape, name=key)

        enc_outputs, enc_last = encode(database, data_batch)
        sample_outputs, greedy_outputs,\
        sample_decode_lengths, greedy_decode_lengths = get_training_outputs(
            database, data_batch, enc_outputs, enc_last, rf_point)

        sample_output_texts = \
            database.target_vocab.map_ids_to_tokens(sample_outputs.sample_id)
        greedy_output_texts = \
            database.target_vocab.map_ids_to_tokens(greedy_outputs.sample_id)

        rf_reward = tf.placeholder(
            shape=(database.batch_size, ), dtype=tf.float32, name='rf_reward')
        train_op = get_optimize_tensor(
            database, data_batch,
            sample_outputs, greedy_outputs,
            sample_decode_lengths, greedy_decode_lengths,
            rf_point, rf_reward)

        saver = tf.train.Saver(tf.global_variables())
        with tf.Session() as sess:
            if epoch_num == 0:
                sess.run(tf.global_variables_initializer())
            else:
                if PRETRAINED and epoch_num == N_XENT:
                    saver.restore(
                        sess, 'pre_trained_models/'
                              'train.ckpt_{}'.format(epoch_num - 1))
                else:
                    saver.restore(
                        sess,
                        LOG_DIR + 'checkpoints/'
                                  'train.ckpt_{}'.format(epoch_num - 1))

            sess.run(tf.local_variables_initializer())
            sess.run(tf.tables_initializer())

            coord = tf.train.Coordinator()
            threads = tf.train.start_queue_runners(sess=sess, coord=coord)

            iterator.switch_to_dataset(sess)

            try:
                while not coord.should_stop():
                    fetched_batch_list = sess.run(
                        data_batch_ori.values(),
                        feed_dict={
                            tx.global_mode(): tf.estimator.ModeKeys.PREDICT})
                    fetched_batch = {}
                    for i in range(len(fetched_batch_list)):
                        fetched_batch[data_batch_ori.keys()[i]] = \
                            fetched_batch_list[i]
                    feed_dict = {}
                    for key in data_batch:
                        feed_dict[data_batch[key]] = fetched_batch[key]
                    feed_dict[tx.global_mode()] = tf.estimator.ModeKeys.TRAIN

                    cur_sample_texts, cur_greedy_texts = sess.run(
                        [sample_output_texts, greedy_output_texts], feed_dict)

                    rewards = []
                    for i in range(database.batch_size):
                        target_text_i = text_polish(
                            fetched_batch['target_text'][i][1:],
                            database.target_vocab.eos_token)
                        sample_text_i = text_polish(
                            cur_sample_texts[i],
                            database.target_vocab.eos_token)
                        greedy_text_i = text_polish(
                            cur_greedy_texts[i],
                            database.target_vocab.eos_token)

                        rewards.append(
                            bleu_score.sentence_bleu(
                                [target_text_i], sample_text_i) -
                            bleu_score.sentence_bleu(
                                [target_text_i], greedy_text_i))

                    feed_dict[rf_reward] = np.array(rewards)
                    cur_loss = sess.run(train_op, feed_dict)
                    print(datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
                          'loss:', cur_loss, file=log_file)
                    log_file.flush()

            except tf.errors.OutOfRangeError:
                print('Done -- epoch limit reached')
            finally:
                coord.request_stop()
            coord.join(threads)

            saved_path = saver.save(
                sess, LOG_DIR + 'checkpoints/train.ckpt_{}'.format(epoch_num))
            print('Model saved in', saved_path)


def valid(epoch_num):
    graph = tf.Graph()
    with graph.as_default():
        database = tx.data.PairedTextData(
            hparams=data_configs[DATASET]['valid'])
        iterator = tx.data.DataIterator(database)
        data_batch = iterator.get_next()

        enc_outputs, enc_last = encode(database, data_batch)
        outputs = \
            get_beam_search_outputs(database, data_batch, enc_outputs, enc_last)
        output_texts = database.target_vocab.map_ids_to_tokens(
            outputs.predicted_ids[:, :, 0])

        hypos = []
        refs = []
        with tf.Session() as sess:
            # sess.run(tf.global_variables_initializer())
            sess.run(tf.local_variables_initializer())
            sess.run(tf.tables_initializer())

            saver = tf.train.Saver(tf.global_variables())
            saver.restore(
                sess, LOG_DIR + 'checkpoints/train.ckpt_{}'.format(epoch_num))

            coord = tf.train.Coordinator()
            threads = tf.train.start_queue_runners(sess=sess, coord=coord)

            iterator.switch_to_dataset(sess)

            try:
                while not coord.should_stop():
                    cur_target_texts, cur_output_texts = sess.run(
                        [data_batch['target_text'][:, 1:], output_texts],
                        feed_dict={
                            tx.global_mode(): tf.estimator.ModeKeys.EVAL})
                    for i in range(database.batch_size):
                        refs.append(
                            [text_polish(
                                cur_target_texts[i],
                                database.target_vocab.eos_token)])
                        hypos.append(
                            text_polish(
                                cur_output_texts[i],
                                database.target_vocab.eos_token))
            except tf.errors.OutOfRangeError:
                print('Done -- epoch limit reached')
            finally:
                coord.request_stop()
            coord.join(threads)
    return bleu_score.corpus_bleu(refs, hypos)


def test(epoch_num):
    graph = tf.Graph()
    with graph.as_default():
        database = tx.data.PairedTextData(
            hparams=data_configs[DATASET]['test'])
        iterator = tx.data.DataIterator(database)
        data_batch = iterator.get_next()

        enc_outputs, enc_last = encode(database, data_batch)
        outputs = \
            get_beam_search_outputs(database, data_batch, enc_outputs, enc_last)
        output_texts = database.target_vocab.map_ids_to_tokens(
            outputs.predicted_ids[:, :, 0])

        hypos = []
        refs = []
        with tf.Session() as sess:
            # sess.run(tf.global_variables_initializer())
            sess.run(tf.local_variables_initializer())
            sess.run(tf.tables_initializer())

            saver = tf.train.Saver(tf.global_variables())
            saver.restore(
                sess, LOG_DIR + 'checkpoints/train.ckpt_{}'.format(epoch_num))

            coord = tf.train.Coordinator()
            threads = tf.train.start_queue_runners(sess=sess, coord=coord)

            iterator.switch_to_dataset(sess)

            try:
                while not coord.should_stop():
                    cur_target_texts, cur_output_texts = sess.run(
                        [data_batch['target_text'][:, 1:], output_texts],
                        feed_dict={
                            tx.global_mode(): tf.estimator.ModeKeys.PREDICT})
                    for i in range(database.batch_size):
                        refs.append(
                            [text_polish(
                                cur_target_texts[i],
                                database.target_vocab.eos_token)])
                        hypos.append(
                            text_polish(
                                cur_output_texts[i],
                                database.target_vocab.eos_token))
            except tf.errors.OutOfRangeError:
                print('Done -- epoch limit reached')
            finally:
                coord.request_stop()
            coord.join(threads)
    return bleu_score.corpus_bleu(refs, hypos)


def mixer():
    result_file = open(LOG_DIR + 'results.txt', 'w')
    if PRETRAINED == False:
        for i in range(N_XENT):
            train(i, 10000)
            valid_scores = []
            test_scores = []
            for j in range(3):
                valid_scores.append(100. * valid(i))
                test_scores.append(100. * test(i))
            print('valid scores:', valid_scores)
            print('test scores:', test_scores)
            print('Epoch', i,
                  'valid:', sum(valid_scores) / 3.,
                  'test:', sum(test_scores) / 3.,
                  file=result_file)
            sys.stdout.flush()
            result_file.flush()
        print('-' * 50, file=result_file)

    epoch_counter = N_XENT
    for rf_point in range(LENGTH_T, -1, -DELTA):
        for j in range(N_XR):
            train(epoch_counter, rf_point)
            valid_scores = []
            test_scores = []
            for j in range(3):
                valid_scores.append(100. * valid(epoch_counter))
                test_scores.append(100. * test(epoch_counter))
            print('valid scores:', valid_scores)
            print('test scores:', test_scores)
            print('Epoch', epoch_counter, '(rf_point: {})'.format(rf_point),
                  'valid:', sum(valid_scores) / 3.,
                  'test:', sum(test_scores) / 3.,
                  file=result_file)
            sys.stdout.flush()
            result_file.flush()
            epoch_counter += 1
        print('-' * 50, file=result_file)


if __name__ == '__main__':
    LOG_DIR = DATASET + \
              '_xent{}_xr{}_t{}_d{}_seq2seq_training_logs/'.format(
                  N_XENT, N_XR, LENGTH_T, DELTA)
    os.system('mkdir ' + LOG_DIR)

    mixer()
