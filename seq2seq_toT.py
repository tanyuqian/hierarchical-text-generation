# coding: utf-8

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import tensorflow as tf
from tensorflow.contrib import seq2seq
from tensorflow.contrib.seq2seq import BeamSearchDecoder

from texar.data import qPairedTextData
from texar.modules import UnidirectionalRNNEncoder
from texar.modules import BidirectionalRNNEncoder
from texar.modules import get_helper
from texar.modules import BasicRNNDecoder
from to_t_helper import ToTHelper
from texar.losses import mle_losses
from texar.core import optimization as opt
from texar import context

import numpy as np

from nltk.translate import bleu_score
from rouge import Rouge

import os
import sys
import math
import argparse
import json
from datetime import datetime

DATASET = None
BEAM_WIDTH = None
NUM_EPOCHS = None
NUM_UNITS = None
LOG_DIR = None
METRIC = None
K = None
SAMPLE_METHOD = None
MAX_DEC_LENGTH = None


def training_model_simple(database, data_batch):
    encoder_hparams = UnidirectionalRNNEncoder.default_hparams()
    encoder_hparams['rnn_cell']['cell']['kwargs']['num_units'] = NUM_UNITS
    encoder_hparams['embedding']['dim'] = NUM_UNITS
    encoder = UnidirectionalRNNEncoder(
        vocab_size=database.source_vocab.vocab_size,
        hparams=encoder_hparams)
    enc_outputs, enc_last = encoder(inputs=data_batch['source_text_ids'])
    decoder_cell = tf.nn.rnn_cell.BasicLSTMCell(num_units=NUM_UNITS)

    return enc_last, decoder_cell


def infer_model_simple(database, data_batch):
    encoder_hparams = UnidirectionalRNNEncoder.default_hparams()
    encoder_hparams['rnn_cell']['cell']['kwargs']['num_units'] = NUM_UNITS
    encoder_hparams['embedding']['dim'] = NUM_UNITS
    encoder = UnidirectionalRNNEncoder(
        vocab_size=database.source_vocab.vocab_size,
        hparams=encoder_hparams)
    enc_outputs, enc_last = encoder(inputs=data_batch['source_text_ids'])
    decoder_cell = tf.nn.rnn_cell.BasicLSTMCell(num_units=NUM_UNITS)

    return seq2seq.tile_batch(enc_last, BEAM_WIDTH), decoder_cell


def training_model_large(database, data_batch):
    DROPOUT = 0.2
    encoder_hparams = BidirectionalRNNEncoder.default_hparams()
    encoder_hparams['rnn_cell_fw']['cell']['kwargs']['num_units'] = NUM_UNITS
    encoder_hparams['rnn_cell_fw']['dropout']['input_keep_prob'] = 1. - DROPOUT
    encoder_hparams['rnn_cell_bw']['cell']['kwargs']['num_units'] = NUM_UNITS
    encoder_hparams['rnn_cell_bw']['dropout']['input_keep_prob'] = 1. - DROPOUT
    encoder_hparams['embedding']['dim'] = NUM_UNITS
    encoder = BidirectionalRNNEncoder(
        vocab_size=database.source_vocab.vocab_size,
        hparams=encoder_hparams)
    enc_outputs, enc_last = encoder(inputs=data_batch['source_text_ids'])

    decoder_cell = tf.nn.rnn_cell.MultiRNNCell(cells=[
        tf.nn.rnn_cell.DropoutWrapper(
            cell=tf.nn.rnn_cell.BasicLSTMCell(num_units=NUM_UNITS),
            input_keep_prob=1. - DROPOUT),
        tf.nn.rnn_cell.DropoutWrapper(
            cell=tf.nn.rnn_cell.BasicLSTMCell(num_units=NUM_UNITS),
            input_keep_prob=1. - DROPOUT)
    ])
    attention_mechanism = seq2seq.LuongAttention(
        num_units=NUM_UNITS,
        memory=BidirectionalRNNEncoder.concat_outputs(enc_outputs),
        memory_sequence_length=data_batch['source_length'],
        scale=True)
    decoder_cell = seq2seq.AttentionWrapper(
        cell=decoder_cell,
        attention_mechanism=attention_mechanism,
        attention_layer_size=NUM_UNITS)

    return decoder_cell.zero_state(
        batch_size=database.batch_size, dtype=tf.float32), decoder_cell


def infer_model_large(database, data_batch):
    encoder_hparams = BidirectionalRNNEncoder.default_hparams()
    encoder_hparams['rnn_cell_fw']['cell']['kwargs']['num_units'] = NUM_UNITS
    encoder_hparams['rnn_cell_bw']['cell']['kwargs']['num_units'] = NUM_UNITS
    encoder_hparams['embedding']['dim'] = NUM_UNITS
    encoder = BidirectionalRNNEncoder(
        vocab_size=database.source_vocab.vocab_size,
        hparams=encoder_hparams)
    enc_outputs, enc_last = encoder(inputs=data_batch['source_text_ids'])

    decoder_cell = tf.nn.rnn_cell.MultiRNNCell(cells=[
        tf.nn.rnn_cell.DropoutWrapper(
            cell=tf.nn.rnn_cell.BasicLSTMCell(num_units=NUM_UNITS),
            input_keep_prob=1.),
        tf.nn.rnn_cell.DropoutWrapper(
            cell=tf.nn.rnn_cell.BasicLSTMCell(num_units=NUM_UNITS),
            input_keep_prob=1.)
    ])

    def tiled(t):
        return seq2seq.tile_batch(t, BEAM_WIDTH)

    attention_mechanism = seq2seq.LuongAttention(
        num_units=NUM_UNITS,
        memory=tiled(BidirectionalRNNEncoder.concat_outputs(enc_outputs)),
        memory_sequence_length=tiled(data_batch['source_length']),
        scale=True)
    decoder_cell = seq2seq.AttentionWrapper(
        cell=decoder_cell,
        attention_mechanism=attention_mechanism,
        attention_layer_size=NUM_UNITS)

    return decoder_cell.zero_state(batch_size=database.batch_size * BEAM_WIDTH,
                                   dtype=tf.float32), decoder_cell


def train(cur_epoch):
    cur_training_log_file = open(
        LOG_DIR + 'training_log_epoch{}.txt'.format(cur_epoch), 'w')

    training_graph = tf.Graph()
    with training_graph.as_default():
        data_hparams = json.load(
            open('data_configs/' + DATASET + '_configs.json'))['train']
        database = qPairedTextData(hparams=data_hparams)
        data_batch = database()

        data_batch_source_text_ids = data_batch['source_text_ids']
        data_batch_source_length = data_batch['source_length']
        data_batch_target_text_ids = data_batch['target_text_ids']
        data_batch_target_length = data_batch['target_length']

        for key, value in data_batch.items():
            data_batch[key] = \
                tf.placeholder(dtype=value.dtype, shape=value.shape)

        if DATASET == 'toycopy':
            dec_initial_state, decoder_cell = \
                training_model_simple(database, data_batch)
        else:
            dec_initial_state, decoder_cell = \
                training_model_large(database, data_batch)

        decoder_hparams = BasicRNNDecoder.default_hparams()
        decoder_hparams['embedding']['dim'] = NUM_UNITS
        decoder_hparams['helper_train']['type'] = 'xxx'
        decoder = BasicRNNDecoder(
            cell=decoder_cell, vocab_size=database.target_vocab.vocab_size,
            hparams=decoder_hparams)

        sampling_start_time = tf.placeholder(dtype=tf.int32, shape=())
        sampling_stop_time = tf.placeholder(dtype=tf.int32, shape=())
        dec_inputs = tf.nn.embedding_lookup(
            decoder.embedding, data_batch['target_text_ids'][:, :-1])
        helper_train = ToTHelper(
            inputs=dec_inputs,
            sequence_length=data_batch['target_length'] - 1,
            embedding=decoder.embedding,
            sample_method=SAMPLE_METHOD,
            sampling_start_time=sampling_start_time,
            sampling_stop_time=sampling_stop_time)

        outputs, final_state, sequence_lengths = decoder(
            helper=helper_train, initial_state=dec_initial_state)

        # print(tf.concat([
        #         data_batch['target_text_ids'][:, 1:sampling_start_time],
        #         tf.to_int64(outputs.sample_id[:, sampling_start_time:sampling_stop_time]),
        #         data_batch['target_text_ids'][:, sampling_stop_time + 1: ]], 1))
        # print(outputs.logits)
        # print(data_batch['target_length'] - 1)
        # exit(0)

        checked_lengths = tf.where(
            tf.greater_equal(sampling_stop_time, data_batch['target_length']),
            tf.zeros_like(data_batch['target_length'] - 1),
            data_batch['target_length'] - 1)

        mle_loss = mle_losses.average_sequence_sparse_softmax_cross_entropy(
            labels=tf.concat([
                data_batch['target_text_ids'][:, 1:sampling_start_time + 1],
                tf.to_int64(outputs.sample_id[:, sampling_start_time:sampling_stop_time]),
                data_batch['target_text_ids'][:, sampling_stop_time + 1: ]], 1),
            logits=outputs.logits,
            sequence_length=checked_lengths)

        # if K != 1:
        #     for i in range(MAX_DEC_LENGTH):
        #         sampling_start_time = i
        #         sampling_stop_time = i + K - 1
        #         helper_train = ToTHelper(
        #             inputs=dec_inputs,
        #             sequence_length=data_batch['target_length'] - 1,
        #             embedding=decoder.embedding,
        #             sample_method=SAMPLE_METHOD,
        #             sampling_start_time=sampling_start_time,
        #             sampling_stop_time=sampling_stop_time)
        #
        #         outputs, final_state, sequence_lengths = decoder(
        #             helper=helper_train, initial_state=dec_initial_state)
        #
        #         checked_lengths = tf.where(
        #             tf.greater_equal(i, data_batch['target_length'] - K + 1),
        #             tf.zeros_like(data_batch['target_length'] - 1),
        #             data_batch['target_length'] - 1)
        #         if i == 0:
        #             labels = tf.concat([
        #                 tf.to_int64(outputs.sample_id[:, i:i + K - 1]),
        #                 data_batch['target_text_ids'][:, K:]
        #             ], 1)
        #         else:
        #             labels = tf.concat([
        #                 data_batch['target_text_ids'][:, 1:i + 1],
        #                 tf.to_int64(outputs.sample_id[:, i:i + K - 1]),
        #                 data_batch['target_text_ids'][:, i + K:]
        #             ], 1)
        #         mle_loss = \
        #             mle_loss + \
        #             mle_losses.average_sequence_sparse_softmax_cross_entropy(
        #                 labels=labels,
        #                 logits=outputs.logits,
        #                 sequence_length=checked_lengths)

        opt_hparams = {
            "optimizer": {
                "type": "AdamOptimizer",
                "kwargs": {}
            }
        }
        train_op, global_step = opt.get_train_op(mle_loss, hparams=opt_hparams)

        saver = tf.train.Saver(tf.global_variables())
        with tf.Session() as sess:
            if cur_epoch == 0:
                sess.run(tf.global_variables_initializer())
            else:
                saver.restore(sess, LOG_DIR +
                              'checkpoints/train.ckpt_{}'.format(cur_epoch - 1))
            sess.run(tf.local_variables_initializer())
            sess.run(tf.tables_initializer())

            coord = tf.train.Coordinator()
            threads = tf.train.start_queue_runners(sess=sess, coord=coord)

            try:
                while not coord.should_stop():
                    cur_source_text_ids, cur_source_length,\
                    cur_target_text_ids, cur_target_length = \
                        sess.run([data_batch_source_text_ids,
                                  data_batch_source_length,
                                  data_batch_target_text_ids,
                                  data_batch_target_length],
                                 feed_dict={context.is_train(): False})

                    cur_total_loss = 0.

                    cur_loss, cur_global_step = \
                        sess.run([train_op, global_step],
                                 feed_dict={
                                     context.is_train(): True,
                                     data_batch['source_text_ids']:
                                         cur_source_text_ids,
                                     data_batch['source_length']:
                                         cur_source_length,
                                     data_batch['target_text_ids']:
                                         cur_target_text_ids,
                                     data_batch['target_length']:
                                         cur_target_length,
                                     sampling_start_time: 1,
                                     sampling_stop_time: 1})
                    cur_total_loss += cur_loss

                    if K >= 2:
                        for i in range(MAX_DEC_LENGTH):
                            cur_loss, cur_global_step = \
                                sess.run([train_op, global_step],
                                         feed_dict={
                                             context.is_train(): True,
                                             data_batch['source_text_ids']:
                                                 cur_source_text_ids,
                                             data_batch['source_length']:
                                                 cur_source_length,
                                             data_batch['target_text_ids']:
                                                 cur_target_text_ids,
                                             data_batch['target_length']:
                                                 cur_target_length,
                                             sampling_start_time: i,
                                             sampling_stop_time: i + K - 1})
                            cur_total_loss += cur_loss

                    cur_training_log_file.write(
                        datetime.now().strftime('%Y-%m-%d %H:%M:%S') +
                        '\tloss: ' + str(cur_total_loss) +
                        '\tglobal_step: ' + str(cur_global_step) + '\n')
                    cur_training_log_file.flush()
                    sys.stdout.flush()

            except tf.errors.OutOfRangeError:
                print('Done -- epoch limit reached')
            finally:
                coord.request_stop()
            coord.join(threads)

            save_path = saver.save(
                sess, LOG_DIR + 'checkpoints/train.ckpt_{}'.format(cur_epoch))
            print('Model saved in' + save_path)
        cur_training_log_file.close()


def valid(cur_epoch):
    cur_valid_log_file = open(
        LOG_DIR + 'valid_log_epoch{}.txt'.format(cur_epoch), 'w')

    valid_graph = tf.Graph()
    with valid_graph.as_default():
        data_hparams = json.load(
            open('data_configs/' + DATASET + '_configs.json'))['valid']
        database = qPairedTextData(hparams=data_hparams)
        data_batch = database()

        if DATASET == 'toycopy':
            dec_initial_state, decoder_cell = \
                infer_model_simple(database, data_batch)
        else:
            dec_initial_state, decoder_cell = \
                infer_model_large(database, data_batch)

        with tf.variable_scope('basic_rnn_decoder'):
            decoder = BeamSearchDecoder(
                cell=decoder_cell,
                embedding=tf.get_variable(
                    name='embedding',
                    shape=[database.target_vocab.vocab_size, NUM_UNITS]),
                start_tokens=[database.target_vocab.bos_token_id
                              ] * database.batch_size,
                end_token=database.target_vocab.eos_token_id,
                beam_width=BEAM_WIDTH,
                initial_state=dec_initial_state,
                output_layer=tf.layers.Dense(database.target_vocab.vocab_size)
            )
            outputs, _, _ = seq2seq.dynamic_decode(
                decoder=decoder, maximum_iterations=60)

        corpus = []
        gene_texts = []
        with tf.Session() as sess:
            # sess.run(tf.global_variables_initializer())
            sess.run(tf.local_variables_initializer())
            sess.run(tf.tables_initializer())

            saver = tf.train.Saver(tf.global_variables())
            saver.restore(
                sess, LOG_DIR + 'checkpoints/train.ckpt_{}'.format(cur_epoch))

            coord = tf.train.Coordinator()
            threads = tf.train.start_queue_runners(sess=sess, coord=coord)

            try:
                while not coord.should_stop():
                    gene_ids, target_text = \
                        sess.run(
                            [outputs.predicted_ids[:, :, 0],
                             data_batch['target_text']],
                            feed_dict={context.is_train(): False})

                    for i in range(database.batch_size):
                        cur_text = []
                        for j in range(1, len(target_text[i])):
                            if target_text[i][j] == \
                                    database.target_vocab.eos_token:
                                break
                            else:
                                cur_text.append(target_text[i][j])
                        if METRIC == 'bleu':
                            corpus.append([cur_text, ])
                        elif METRIC == 'rouge':
                            corpus.append(' '.join(cur_text).decode('utf-8'))
                    for i in range(database.batch_size):
                        cur_text = []
                        for j in range(len(gene_ids[i])):
                            if gene_ids[i][j] == \
                                    database.target_vocab.eos_token_id:
                                break
                            else:
                                cur_text.append(
                                    database.target_vocab.id_to_token_map_py[
                                        gene_ids[i][j]])
                        if METRIC == 'bleu':
                            gene_texts.append(cur_text)
                        elif METRIC == 'rouge':
                            gene_texts.append(
                                ' '.join(cur_text).decode('utf-8'))
            except tf.errors.OutOfRangeError:
                print('Done -- epoch limit reached')
            finally:
                coord.request_stop()
            coord.join(threads)

        if METRIC == 'bleu':
            print('---------------------------------------')
            for i in range(len(corpus)):
                for j in range(len(corpus[i])):
                    print(corpus[i][j])
                print(gene_texts[i])
                sys.stdout.flush()
            print('---------------------------------------')
            sys.stdout.flush()
            score = 100 * bleu_score.corpus_bleu(corpus, gene_texts)
            cur_valid_log_file.write(
                'valid BLEU after epoch{}: '.format(cur_epoch) +
                str(score) + '\n')
            cur_valid_log_file.close()
            return score
        elif METRIC == 'rouge':
            print('---------------------------------------')
            for i in range(len(corpus)):
                print(corpus[i].replace(u'\xa0', u' '))
                print(gene_texts[i].replace(u'\xa0', u' '))
                sys.stdout.flush()
            print('---------------------------------------')
            sys.stdout.flush()

            rouge = Rouge()
            score = 100. * rouge.get_scores(
                hyps=gene_texts, refs=corpus, avg=True)['rouge-2']['r']
            cur_valid_log_file.write(
                'valid ROUGE-2 after epoch{}: '.format(cur_epoch) +
                str(score) + '\n')
            cur_valid_log_file.close()
            return score


def test(cur_epoch):
    cur_testing_log_file = open(
        LOG_DIR + 'testing_log_epoch{}.txt'.format(cur_epoch), 'w')

    testing_graph = tf.Graph()
    with testing_graph.as_default():
        data_hparams = json.load(
            open('data_configs/' + DATASET + '_configs.json'))['test']
        database = qPairedTextData(hparams=data_hparams)
        data_batch = database()

        if DATASET == 'toycopy':
            dec_initial_state, decoder_cell = \
                infer_model_simple(database, data_batch)
        else:
            dec_initial_state, decoder_cell = \
                infer_model_large(database, data_batch)

        with tf.variable_scope('basic_rnn_decoder'):
            decoder = BeamSearchDecoder(
                cell=decoder_cell,
                embedding=tf.get_variable(
                    name='embedding',
                    shape=[database.target_vocab.vocab_size, NUM_UNITS]),
                start_tokens=[database.target_vocab.bos_token_id
                              ] * database.batch_size,
                end_token=database.target_vocab.eos_token_id,
                beam_width=BEAM_WIDTH,
                initial_state=dec_initial_state,
                output_layer=tf.layers.Dense(database.target_vocab.vocab_size)
            )
            outputs, _, _ = seq2seq.dynamic_decode(
                decoder=decoder, maximum_iterations=60)

        corpus = []
        gene_texts = []
        with tf.Session() as sess:
            # sess.run(tf.global_variables_initializer())
            sess.run(tf.local_variables_initializer())
            sess.run(tf.tables_initializer())

            saver = tf.train.Saver(tf.global_variables())
            saver.restore(
                sess, LOG_DIR + 'checkpoints/train.ckpt_{}'.format(cur_epoch))

            coord = tf.train.Coordinator()
            threads = tf.train.start_queue_runners(sess=sess, coord=coord)

            try:
                while not coord.should_stop():
                    gene_ids, target_text = \
                        sess.run(
                            [outputs.predicted_ids[:, :, 0],
                             data_batch['target_text']],
                            feed_dict={context.is_train(): False})

                    for i in range(database.batch_size):
                        cur_text = []
                        for j in range(1, len(target_text[i])):
                            if target_text[i][j] == \
                                    database.target_vocab.eos_token:
                                break
                            else:
                                cur_text.append(target_text[i][j])
                        if METRIC == 'bleu':
                            corpus.append([cur_text, ])
                        elif METRIC == 'rouge':
                            corpus.append(' '.join(cur_text).decode('utf-8'))
                    for i in range(database.batch_size):
                        cur_text = []
                        for j in range(len(gene_ids[i])):
                            if gene_ids[i][j] == \
                                    database.target_vocab.eos_token_id:
                                break
                            else:
                                cur_text.append(
                                    database.target_vocab.id_to_token_map_py[
                                        gene_ids[i][j]])
                        if METRIC == 'bleu':
                            gene_texts.append(cur_text)
                        elif METRIC == 'rouge':
                            gene_texts.append(
                                ' '.join(cur_text).decode('utf-8'))
            except tf.errors.OutOfRangeError:
                print('Done -- epoch limit reached')
            finally:
                coord.request_stop()
            coord.join(threads)

        if METRIC == 'bleu':
            print('---------------------------------------')
            for i in range(len(corpus)):
                for j in range(len(corpus[i])):
                    print(corpus[i][j])
                print(gene_texts[i])
                sys.stdout.flush()
            print('---------------------------------------')
            sys.stdout.flush()
            score = 100 * bleu_score.corpus_bleu(corpus, gene_texts)
            cur_testing_log_file.write(
                'test BLEU after epoch{}: '.format(cur_epoch) +
                str(score) + '\n')
            cur_testing_log_file.close()
            return score
        elif METRIC == 'rouge':
            print('---------------------------------------')
            for i in range(len(corpus)):
                print(corpus[i].replace(u'\xa0', u' '))
                print(gene_texts[i].replace(u'\xa0', u' '))
                sys.stdout.flush()
            print('---------------------------------------')
            sys.stdout.flush()

            rouge = Rouge()
            score = 100. * rouge.get_scores(
                hyps=gene_texts, refs=corpus, avg=True)['rouge-2']['r']
            cur_testing_log_file.write(
                'test ROUGE-2 after epoch{}: '.format(cur_epoch) +
                str(score) + '\n')
            cur_testing_log_file.close()
            return score


def parse_args():
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument(
        '--dataset', type=str, default=None, help='iwslt14|textsum|toycopy')
    arg_parser.add_argument('--k', type=int, default=1)
    arg_parser.add_argument(
        '--sample_method',
        type=str, default='argmax', help='argmax|categorical')
    arg_parser.add_argument(
        '--metric', type=str, default='bleu', help='bleu|rouge')

    return arg_parser.parse_args()


if __name__ == '__main__':
    args = parse_args()

    DATASET = args.dataset
    K = args.k
    SAMPLE_METHOD = args.sample_method
    METRIC = args.metric

    if DATASET == 'toycopy':
        NUM_UNITS = 128
        NUM_EPOCHS = 400
        BEAM_WIDTH = 4
        MAX_DEC_LENGTH = 22
    else:
        NUM_UNITS = 512
        BEAM_WIDTH = 10
        if DATASET == 'iwslt14':
            NUM_EPOCHS = 10
            MAX_DEC_LENGTH = 25
        else:
            NUM_EPOCHS = 20
            MAX_DEC_LENGTH = 15

    LOG_DIR = DATASET + '_ToT_training_log' + \
              '_K' + str(K) + '_' + SAMPLE_METHOD
    LOG_DIR += '/'

    os.system('mkdir ' + LOG_DIR)
    results_file = open(LOG_DIR + 'results.txt', 'w')

    max_valid_score = 0.
    for i in range(NUM_EPOCHS):
        train(i)

        scores = []
        results_file.write('valid epoch {}'.format(i) + '\n')
        for j in range(3):
            scores.append(valid(i))
            results_file.write(str(scores[-1]) + '\n')
            results_file.flush()
        max_valid_score = max(max_valid_score, sum(scores) / 3.)
        results_file.write('avg score: ' + str(sum(scores) / 3.) +
                           ' max ever: ' + str(max_valid_score) + '\n')
        results_file.write('------------------------------\n')
        results_file.flush()

        scores = []
        results_file.write('test epoch {}'.format(i) + '\n')
        for j in range(3):
            scores.append(test(i))
            results_file.write(str(scores[-1]) + '\n')
            results_file.flush()
        results_file.write('avg score: ' + str(sum(scores) / 3.) + '\n')
        results_file.write('------------------------------\n')
        results_file.flush()
