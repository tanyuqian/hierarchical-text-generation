from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import collections

import tensorflow as tf
from tensorflow.python.util import nest
from tensorflow.python.ops.distributions import categorical
from tensorflow.python.ops import math_ops

from texar.modules.decoders.rnn_decoder_base import RNNDecoderBase


class MagicRNNDecoderOutput(collections.namedtuple(
    "MagicRNNDecoderOutput",
    ("logits", 'sampled_ids0', 'logits_follow1',
     'sampled_ids1', 'logits_follow2',
     'sampled_ids2', 'logits_follow3',
     "sample_id", "cell_output"))):
    pass


class MagicRNNDecoder(RNNDecoderBase):
    def __init__(self, cell=None, embedding=None, vocab_size=None,
                 output_layer=None, hparams=None):
        RNNDecoderBase.__init__(
            self, cell, embedding, vocab_size, output_layer, hparams)
        self.n_samples = self._hparams.n_samples
        self.sample_method = self._hparams.sample_method
        self.magic_length = self._hparams.magic_length
        with tf.variable_scope(self.variable_scope):
            self._output_layer_follow1 = tf.layers.Dense(
                units=self._vocab_size, name='dense_follow1')
            self._output_layer_follow2 = tf.layers.Dense(
                units=self._vocab_size, name='dense_follow2')
            self._output_layer_follow3 = tf.layers.Dense(
                units=self._vocab_size, name='dense_follow3')

    @staticmethod
    def default_hparams():
        hparams = RNNDecoderBase.default_hparams()
        hparams["name"] = "magic_rnn_decoder"
        hparams['n_samples'] = 1
        hparams['sample_method'] = 'argmax'
        hparams['magic_length'] = 1
        return hparams

    def initialize(self, name=None):
        return self._helper.initialize() + (self._initial_state,)

    def sample(self, logits):
        if self.sample_method == 'categorical':
            sampler = categorical.Categorical(logits=logits)
            return sampler.sample()
        elif self.sample_method == 'argmax':
            return math_ops.cast(math_ops.argmax(logits, axis=-1), tf.int32)
        else:
            raise ValueError

    def step(self, time, inputs, state, name=None):
        cell_outputs, cell_state = self._cell(inputs, state)
        logits = self._output_layer(cell_outputs)
        sample_ids = self._helper.sample(
            time=time, outputs=logits, state=cell_state)
        (finished, next_inputs, next_state) = self._helper.next_inputs(
            time=time,
            outputs=logits,
            state=cell_state,
            sample_ids=sample_ids)

        cc_sampled_ids0 = None
        cc_logits_follow1 = None
        cc_sampled_ids1 = None
        cc_logits_follow2 = None
        cc_sampled_ids2 = None
        cc_logits_follow3 = None

        for i in range(self.n_samples):
            # follow1
            if self.magic_length >= 2:
                sampled_ids0 = self.sample(logits=logits)
                follow_inputs = sampled_ids0
                if self.embedding is not None:
                    follow_inputs = \
                        tf.nn.embedding_lookup(self.embedding, follow_inputs)
                cell_outputs_follow1, cell_state_follow1 = \
                    self._cell(follow_inputs, next_state)
                logits_follow1 = \
                    self._output_layer_follow1(cell_outputs_follow1)
            else:
                sampled_ids0 = tf.zeros_like(sample_ids)
                logits_follow1 = tf.zeros_like(logits)

            # follow2
            if self.magic_length >= 3:
                sampled_ids1 = self.sample(logits=logits_follow1)
                follow_inputs = sampled_ids1
                if self.embedding is not None:
                    follow_inputs = \
                        tf.nn.embedding_lookup(self.embedding, follow_inputs)
                cell_outputs_follow2, cell_state_follow2 = \
                    self._cell(follow_inputs, cell_state_follow1)
                logits_follow2 = \
                    self._output_layer_follow2(cell_outputs_follow2)
            else:
                sampled_ids1 = tf.zeros_like(sample_ids)
                logits_follow2 = tf.zeros_like(logits)

            # follow3
            if self.magic_length >= 4:
                sampled_ids2 = self.sample(logits=logits_follow2)
                follow_inputs = sampled_ids2
                if self.embedding is not None:
                    follow_inputs = \
                        tf.nn.embedding_lookup(self.embedding, follow_inputs)
                cell_outputs_follow3, cell_state_follow3 = \
                    self._cell(follow_inputs, cell_state_follow2)
                logits_follow3 = \
                    self._output_layer_follow3(cell_outputs_follow3)
            else:
                sampled_ids2 = tf.zeros_like(sample_ids)
                logits_follow3 = tf.zeros_like(logits)

            if i == 0:
                cc_sampled_ids0 = tf.expand_dims(sampled_ids0, -1)
                cc_logits_follow1 = tf.expand_dims(logits_follow1, -1)
                cc_sampled_ids1 = tf.expand_dims(sampled_ids1, -1)
                cc_logits_follow2 = tf.expand_dims(logits_follow2, -1)
                cc_sampled_ids2 = tf.expand_dims(sampled_ids2, -1)
                cc_logits_follow3 = tf.expand_dims(logits_follow3, -1)
            else:
                cc_sampled_ids0 = tf.concat(
                    [cc_sampled_ids0, tf.expand_dims(sampled_ids0, -1)], -1)
                cc_logits_follow1 = tf.concat(
                    [cc_logits_follow1, tf.expand_dims(logits_follow1, -1)], -1)
                cc_sampled_ids1 = tf.concat(
                    [cc_sampled_ids1, tf.expand_dims(sampled_ids1, -1)], -1)
                cc_logits_follow2 = tf.concat(
                    [cc_logits_follow2, tf.expand_dims(logits_follow2, -1)], -1)
                cc_sampled_ids2 = tf.concat(
                    [cc_sampled_ids2, tf.expand_dims(sampled_ids2, -1)], -1)
                cc_logits_follow3 = tf.concat(
                    [cc_logits_follow3, tf.expand_dims(logits_follow3, -1)], -1)

        outputs = MagicRNNDecoderOutput(
            logits=logits,
            sampled_ids0=cc_sampled_ids0,
            logits_follow1=cc_logits_follow1,
            sampled_ids1=cc_sampled_ids1,
            logits_follow2=cc_logits_follow2,
            sampled_ids2=cc_sampled_ids2,
            logits_follow3=cc_logits_follow3,
            sample_id=sample_ids,
            cell_output=cell_outputs)

        return outputs, next_state, next_inputs, finished

    def finalize(self, outputs, final_state, sequence_lengths):
        return outputs, final_state

    @property
    def output_size(self):
        """Output size of one step.
        """
        return MagicRNNDecoderOutput(
            logits=self._rnn_output_size(),
            sampled_ids0=tf.TensorShape(
                self._helper.sample_ids_shape.as_list() + [self.n_samples, ]),
            logits_follow1=tf.TensorShape(
                self._rnn_output_size().as_list() + [self.n_samples, ]),
            sampled_ids1=tf.TensorShape(
                self._helper.sample_ids_shape.as_list() + [self.n_samples, ]),
            logits_follow2=tf.TensorShape(
                self._rnn_output_size().as_list() + [self.n_samples, ]),
            sampled_ids2=tf.TensorShape(
                self._helper.sample_ids_shape.as_list() + [self.n_samples, ]),
            logits_follow3=tf.TensorShape(
                self._rnn_output_size().as_list() + [self.n_samples, ]),
            sample_id=self._helper.sample_ids_shape,
            cell_output=self._cell.output_size)

    @property
    def output_dtype(self):
        """Types of output of one step.
        """
        # Assume the dtype of the cell is the output_size structure
        # containing the input_state's first component's dtype.
        # Return that structure and the sample_ids_dtype from the helper.
        dtype = nest.flatten(self._initial_state)[0].dtype
        return MagicRNNDecoderOutput(
            logits=nest.map_structure(lambda _: dtype, self._rnn_output_size()),
            sampled_ids0=nest.map_structure(
                lambda _: self._helper.sample_ids_dtype,
                self.output_size.sampled_ids0),
            logits_follow1=nest.map_structure(
                lambda _: dtype, self.output_size.logits_follow1),
            sampled_ids1=nest.map_structure(
                lambda _: self._helper.sample_ids_dtype,
                self.output_size.sampled_ids1),
            logits_follow2=nest.map_structure(
                lambda _: dtype, self.output_size.logits_follow2),
            sampled_ids2=nest.map_structure(
                lambda _: self._helper.sample_ids_dtype,
                self.output_size.sampled_ids1),
            logits_follow3=nest.map_structure(
                lambda _: dtype, self.output_size.logits_follow2),
            sample_id=self._helper.sample_ids_dtype,
            cell_output=nest.map_structure(
                lambda _: dtype, self._cell.output_size))
