from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import abc

import six

from tensorflow.contrib.seq2seq.python.ops import decoder
from tensorflow.python.framework import dtypes
from tensorflow.python.framework import ops
from tensorflow.python.framework import tensor_shape
from tensorflow.python.ops import array_ops
from tensorflow.python.ops import control_flow_ops
from tensorflow.python.ops import embedding_ops
from tensorflow.python.ops import gen_array_ops
from tensorflow.python.ops import math_ops
from tensorflow.python.ops import tensor_array_ops
from tensorflow.python.ops.distributions import bernoulli
from tensorflow.python.ops.distributions import categorical
from tensorflow.python.util import nest

import tensorflow as tf
import tensorflow.contrib.seq2seq as seq2seq


class ToTHelper(seq2seq.TrainingHelper):
  """A training helper that adds scheduled sampling.

  Returns -1s for sample_ids where no sampling took place; valid sample id
  values elsewhere.
  """

  def __init__(self, inputs, sequence_length, embedding,
               sampling_start_time, sampling_stop_time, sample_method,
               time_major=False, seed=None, scheduling_seed=None, name=None):
    with ops.name_scope(name, "ScheduledEmbeddingSamplingWrapper",
                        [embedding, sampling_start_time, sampling_stop_time]):
      if callable(embedding):
        self._embedding_fn = embedding
      else:
        self._embedding_fn = (
            lambda ids: embedding_ops.embedding_lookup(embedding, ids))
      self._sampling_start_time = ops.convert_to_tensor(
          sampling_start_time, name="sampling_start_time", dtype=dtypes.int32)
      self._sampling_stop_time = ops.convert_to_tensor(
          sampling_stop_time, name="sampling_stop_time", dtype=dtypes.int32)
      self.sample_method = sample_method
      if self._sampling_start_time.get_shape().ndims not in (0, 1):
        raise ValueError(
            "sampling_probability must be either a scalar or a vector. "
            "saw shape: %s" % (self._sampling_probability.get_shape()))
      self._seed = seed
      self._scheduling_seed = scheduling_seed
      super(ToTHelper, self).__init__(
          inputs=inputs,
          sequence_length=sequence_length,
          time_major=time_major,
          name=name)

  def initialize(self, name=None):
    return super(ToTHelper, self).initialize(name=name)

  def sample(self, time, outputs, state, name=None):
    with ops.name_scope(name, "ScheduledEmbeddingTrainingHelperSample",
                        [time, outputs, state]):
      # Return -1s where we did not sample, and sample_ids elsewhere
      if self.sample_method == 'categorical':
          sample_id_sampler = categorical.Categorical(logits=outputs)
          return tf.cond(
              tf.logical_and(
                  tf.greater_equal(time, self._sampling_start_time),
                  tf.less(time, self._sampling_stop_time)),
              lambda : sample_id_sampler.sample(seed=self._seed),
              lambda: gen_array_ops.fill([self.batch_size], -1))
      elif self.sample_method == 'argmax':
          return tf.cond(
              tf.logical_and(
                  tf.greater_equal(time, self._sampling_start_time),
                  tf.less(time, self._sampling_stop_time)),
              lambda: math_ops.cast(
                  math_ops.argmax(outputs, axis=-1), tf.int32),
              lambda: gen_array_ops.fill([self.batch_size], -1))
      else:
          raise ValueError

  def next_inputs(self, time, outputs, state, sample_ids, name=None):
    with ops.name_scope(name, "ToTHelperNextInputs",
                        [time, outputs, state, sample_ids]):
      (finished, base_next_inputs, state) = (
          super(ToTHelper, self).next_inputs(
              time=time,
              outputs=outputs,
              state=state,
              sample_ids=sample_ids,
              name=name))

      def maybe_sample():
        """Perform scheduled sampling."""
        where_sampling = math_ops.cast(
            array_ops.where(sample_ids > -1), dtypes.int32)
        where_not_sampling = math_ops.cast(
            array_ops.where(sample_ids <= -1), dtypes.int32)
        sample_ids_sampling = array_ops.gather_nd(sample_ids, where_sampling)
        inputs_not_sampling = array_ops.gather_nd(
            base_next_inputs, where_not_sampling)
        sampled_next_inputs = self._embedding_fn(sample_ids_sampling)
        base_shape = array_ops.shape(base_next_inputs)
        return (array_ops.scatter_nd(indices=where_sampling,
                                     updates=sampled_next_inputs,
                                     shape=base_shape)
                + array_ops.scatter_nd(indices=where_not_sampling,
                                       updates=inputs_not_sampling,
                                       shape=base_shape))

      all_finished = math_ops.reduce_all(finished)
      next_inputs = control_flow_ops.cond(
          all_finished, lambda: base_next_inputs, maybe_sample)
      return (finished, next_inputs, state)
