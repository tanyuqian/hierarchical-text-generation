from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import abc

import six

from tensorflow.contrib.seq2seq.python.ops import decoder
from tensorflow.python.framework import dtypes
from tensorflow.python.framework import ops
from tensorflow.python.framework import tensor_shape
from tensorflow.python.ops import array_ops
from tensorflow.python.ops import control_flow_ops
from tensorflow.python.ops import embedding_ops
from tensorflow.python.ops import gen_array_ops
from tensorflow.python.ops import math_ops
from tensorflow.python.ops import tensor_array_ops
from tensorflow.python.ops.distributions import bernoulli
from tensorflow.python.ops.distributions import categorical
from tensorflow.python.util import nest

import tensorflow as tf
from tensorflow.contrib import seq2seq


class MixerSampleHelper(seq2seq.SampleEmbeddingHelper):
    def __init__(self, embedding, start_tokens, end_token,
                 inputs, rf_point,
                 softmax_temperature=None, seed=None):
        super(MixerSampleHelper, self).__init__(
            embedding, start_tokens, end_token, softmax_temperature, seed)
        self.inputs = inputs
        self.rf_point = rf_point

    def sample(self, time, outputs, state, name=None):
        """sample for SampleEmbeddingHelper."""

        if not isinstance(outputs, ops.Tensor):
            raise TypeError("Expected outputs to be a single Tensor, got: %s" %
                            type(outputs))

        if self._softmax_temperature is None:
            logits = outputs
        else:
            logits = outputs / self._softmax_temperature

        sample_id_sampler = categorical.Categorical(logits=logits)
        sample_ids = sample_id_sampler.sample(seed=self._seed)

        return tf.cond(
            tf.greater_equal(time, self.rf_point),
            lambda : sample_ids,
            lambda : tf.to_int32(self.inputs[:, time]))


class MixerGreedyHelper(seq2seq.GreedyEmbeddingHelper):
    def __init__(self, embedding, start_tokens, end_token,
                 inputs, rf_point):
        super(MixerGreedyHelper, self).__init__(
            embedding, start_tokens, end_token)
        self.inputs = inputs
        self.rf_point = rf_point

    def sample(self, time, outputs, state, name=None):
        # Outputs are logits, use argmax to get the most probable id
        if not isinstance(outputs, ops.Tensor):
            raise TypeError("Expected outputs to be a single Tensor, got: %s" %
                            type(outputs))
        sample_ids = math_ops.argmax(outputs, axis=-1, output_type=dtypes.int32)

        return tf.cond(
            tf.greater_equal(time, self.rf_point),
            lambda: sample_ids,
            lambda: tf.to_int32(self.inputs[:, time]))
